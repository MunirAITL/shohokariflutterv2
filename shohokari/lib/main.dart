import 'dart:io';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserDevice.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'Mixin.dart';
import 'config/cfg/AppDefine.dart';
import 'config/server/Server.dart';
import 'config/theme/theme_types/app_themes.dart';
import 'data/network/NetworkMgr.dart';
import 'data/translations/LanguageTranslations.dart';
import 'view/splash/splash_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();

  FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(true);
  FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;

  //  device settings
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  //  error catching
  FlutterError.onError = (FlutterErrorDetails details) async {
    print(
        "************************** FlutterErrorDetails" + details.toString());
  };

  EasyLoading.instance
    ..displayDuration = const Duration(seconds: 3)
    ..loadingStyle = EasyLoadingStyle.custom
    ..textColor = Colors.white
    ..textStyle = TextStyle(fontFamily: "Fieldwork", fontSize: 17)
    ..backgroundColor = MyTheme.redColor
    ..indicatorColor = Colors.white
    ..maskColor = Colors.black
    ..indicatorType = EasyLoadingIndicatorType.fadingCircle
    ..maskType = EasyLoadingMaskType.clear
    ..userInteractions = false;

  HttpOverrides.global = MyHttpOverrides();

  //  get device info
  userDevice.setUserDevice();

  runApp(MyApp());
}

class MyApp extends StatelessWidget with Mixin {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        debugShowCheckedModeBanner: false,
        translations: LngTR(),
        locale: Get.deviceLocale,
        fallbackLocale:
            Locale(AppDefine.LANGUAGE, AppDefine.GEO_CODE.toUpperCase()),
        smartManagement: SmartManagement.full,
        enableLog: Server.isTest,
        defaultTransition: Transition.fade,
        title: AppDefine.APP_NAME,
        theme: MyTheme.themeData,
        darkTheme: AppThemes.dark,
        //theme: AppThemes.dark,
        themeMode: ThemeMode.system,
        builder: (context, widget) {
          widget = EasyLoading.init()(context, widget);
          return widget;
        },
        home: SplashPage());
    //home: Test());
  }
}

class Test extends StatefulWidget {
  @override
  State createState() => _TestState();
}

class _TestState extends State<Test> with Mixin {
  @override
  void initState() {
    super.initState();
    //wsSrv();
    Future.delayed(Duration.zero, () async {
      // ?s over, navigate to a new page
      Get.to(() => WebScreen(
            //'http://192.168.1.100/mm/'
            url: 'http://192.168.24.181/test.php',
            title: 'test',
          ));
    });
  }

  @mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        body: Container(),
      ),
    );
  }
}
