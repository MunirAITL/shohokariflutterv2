import 'dart:developer';
import 'dart:io';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/server/ResCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/res/ResolutionAPIModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/dropdown/DropDownListDialog.dart';
import 'package:aitl/view/widgets/picker/CamPicker.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'resolution_base.dart';

class ResolutionScreen extends StatefulWidget {
  @override
  State createState() => _ResolutionScreenState();
}

class _ResolutionScreenState extends BaseResolutionStatefull<ResolutionScreen>
    with APIStateListener {
  final cmt = TextEditingController();
  List<MediaUploadFilesModel> listMediaUploadFilesModel = [];

  //  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.media_upload_file &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            listMediaUploadFilesModel.add(model.responseData.images[0]);
            setState(() {});
          } else {
            final err = model.errorMessages.upload_pictures[0].toString();
            showToast(context: context, msg: err);
          }
        }
      } else if (apiState.type == APIType.resolution &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            cmt.text = "";
            listMediaUploadFilesModel.clear();
            final msg = "resolution_post_msg"
                .tr; //model.messages.resolution_post[0].toString();
            showToast(context: context, msg: msg, which: 1);
            setState(() {});
          } else {
            final err = model.errorMessages.upload_pictures[0].toString();
            showToast(context: context, msg: err);
          }
        }
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    cmt.dispose();
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }

    listMediaUploadFilesModel = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          title: UIHelper().drawAppbarTitle(title: 'contact_us'.tr),
          centerTitle: false,
        ),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return ListView(
      shrinkWrap: true,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
          child: DropDownListDialog(
            context: context,
            title: opt.title,
            ddTitleList: dd,
            callback: (optionItem) {
              opt = optionItem;
              setState(() {});
            },
          ),
          /*DropDownPicker(
            cap: "Support Ticket Type",
            titleColor: MyTheme.gray4Color,
            txtColor: MyTheme.gray4Color,
            itemSelected: opt,
            dropListModel: dd,
            onOptionSelected: (optionItem) {
              opt = optionItem;
              setState(() {});
            },
          ),*/
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
          child: Txt(
              txt: "how_help_you".tr,
              txtColor: MyTheme.gray4Color,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: false),
        ),
        Container(
          margin: const EdgeInsets.only(left: 20.0, right: 20, top: 10),
          decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.all(Radius.circular(10))),
          child: TextField(
            controller: cmt,
            minLines: 5,
            maxLines: 10,
            //expands: true,
            autocorrect: false,
            maxLength: 500,
            keyboardType: TextInputType.multiline,
            style: TextStyle(
              color: Colors.black,
              fontSize: getTxtSize(context: context, txtSize: MyTheme.txtSize),
            ),
            decoration: InputDecoration(
              counter: Offstage(),
              hintText: 'contact_fragment_description_hint'.tr,
              hintStyle: TextStyle(color: Colors.grey),
              //labelText: 'Your message',
              border: InputBorder.none,
              focusedBorder: InputBorder.none,
              enabledBorder: InputBorder.none,
              errorBorder: InputBorder.none,
              disabledBorder: InputBorder.none,
              contentPadding:
                  EdgeInsets.only(left: 15, bottom: 11, top: 11, right: 15),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
          child: Txt(
              txt: "attachments".tr +
                  " - " +
                  listMediaUploadFilesModel.length.toString() +
                  ' ' +
                  'files_added'.tr,
              txtColor: MyTheme.gray4Color,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: false),
        ),
        for (MediaUploadFilesModel model in listMediaUploadFilesModel)
          Container(
            child: ListTile(
              dense: true,
              visualDensity: VisualDensity(horizontal: 0, vertical: -4),
              leading: IconButton(
                  icon: Icon(
                    Icons.remove_circle,
                    color: Colors.red,
                  ),
                  onPressed: () {
                    listMediaUploadFilesModel.remove(model);
                    setState(() {});
                  }),
              title: Align(
                alignment: Alignment(-1.2, 0),
                child: Padding(
                  padding: const EdgeInsets.only(top: 5),
                  child: Txt(
                      txt: model.url.split('/').last ?? '',
                      txtColor: MyTheme.gray4Color,
                      txtSize: MyTheme.txtSize - .5,
                      txtAlign: TextAlign.center,
                      isBold: false),
                ),
              ),
            ),
          ),
        Padding(
          padding: const EdgeInsets.all(20),
          child: DottedBorder(
            borderType: BorderType.RRect,
            radius: Radius.circular(12),
            padding: EdgeInsets.all(6),
            color: Colors.grey,
            strokeWidth: 3,
            child: GestureDetector(
              onTap: () async {
                if (listMediaUploadFilesModel.length >
                    AppConfig.totalUploadLimit - 1) {
                  showToast(
                    context: context,
                    msg: "max_file_limit".tr +
                        " " +
                        AppConfig.totalUploadLimit.toString() +
                        " " +
                        'files'.tr,
                  );
                } else {
                  CamPicker().showCamDialog(
                    context: context,
                    isRear: false,
                    callback: (File path) async {
                      if (path != null) {
                        await APIViewModel().upload(
                          context: context,
                          apiState: APIState(APIType.media_upload_file,
                              this.runtimeType, null),
                          file: path,
                        );
                      }
                    },
                  );
                }
              },
              child: Container(
                height: 50,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.attach_file,
                      color: MyTheme.gray4Color,
                      size: 30,
                    ),
                    Txt(
                        txt: "add_upto5_files".tr,
                        txtColor: MyTheme.gray4Color,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.center,
                        isBold: false),
                  ],
                ),
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: MMBtn(
            txt: "submit".tr,
            width: getW(context),
            height: getHP(context, MyTheme.btnHpa),
            radius: 10,
            callback: () {
              if (opt.id == null) {
                showToast(
                  context: context,
                  msg: "choose_ticket_type".tr,
                );
                return;
              } else if (cmt.text.trim().length == 0) {
                showToast(context: context, msg: "pls_enter_desc".tr);
                return;
              }
              List<String> listFileUrl = [];
              for (MediaUploadFilesModel model in listMediaUploadFilesModel) {
                listFileUrl.add(model.url);
              }

              APIViewModel().req<ResolutionAPIModel>(
                context: context,
                apiState: APIState(APIType.resolution, this.runtimeType, null),
                url: ResCfg.RES_POST_URL,
                reqType: ReqType.Post,
                param: {
                  "Description":
                      'contact_from_app'.tr + ' : ' + cmt.text.trim(),
                  "InitiatorId": userData.userModel.id,
                  "Remarks": "",
                  "ResolutionType": "Other",
                  "ServiceDate": DateTime.now().toString(),
                  "Status": 101,
                  "Title": opt.title,
                  "FileUrl": listFileUrl.join(','),
                  "UserId": userData.userModel.id,
                },
              );
            },
          ),
        ),
        SizedBox(height: 50),
      ],
    );
  }
}
