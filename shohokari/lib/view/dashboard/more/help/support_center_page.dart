import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/audio/audio_mgr.dart';
import 'package:aitl/view/dashboard/more/resolutions/resolution_page.dart';
import 'package:aitl/view/widgets/btn/MMArrowBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';
import 'help_base.dart';

class SupportCenterPage extends StatefulWidget {
  const SupportCenterPage({Key key}) : super(key: key);

  @override
  State createState() => _SupportCenterPageState();
}

class _SupportCenterPageState extends BaseHelpStatefull<SupportCenterPage>
    with StateListener {
  //  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  StateProvider _stateProvider;
  @override
  void onStateChanged(state, data) async {
    try {
      if (state == ObserverState.STATE_AUDIO_START &&
          data == this.runtimeType) {
        await audioController.getAudio();
        if (audioController.isAudio.value) AudioMgr().play("more_help");
      } else if (state == ObserverState.STATE_AUDIO_STOP) {
        AudioMgr().stop();
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    try {
      AudioMgr().stop2();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
      StateProvider().notify(ObserverState.STATE_AUDIO_START, this.runtimeType);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          elevation: MyTheme.appbarElevation,
          title: UIHelper().drawAppbarTitle(title: 'support_activity_title'.tr),
          centerTitle: false,
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: MyTheme.redColor,
          child: Icon(
            Icons.add,
            color: Colors.white, //The color which you want set.
          ),
          onPressed: () => {
            Get.to(
              () => ResolutionScreen(),
            )
          },
        ),
        body: drawLayout(),
      ),
    );
  }

  @override
  drawLayout() {
    return drawSupportButtons();
  }

  drawSupportButtons() {
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(20),
            child: MMArrowBtn(
              txt: "email_txt".tr + ": " + AppDefine.SUPPORT_EMAIL,
              icon: null,
              width: getW(context),
              height: getHP(context, MyTheme.btnHpa),
              radius: 20,
              callback: () {
                launch("mailto:" + AppDefine.SUPPORT_EMAIL);
              },
            ),
          ),
          Txt(
              txt: "or".tr,
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: false),
          Padding(
            padding:
                const EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 10),
            child: MMArrowBtn(
              txt: "call".tr + ": " + AppDefine.SUPPORT_CALL1,
              icon: null,
              width: getW(context),
              height: getHP(context, MyTheme.btnHpa),
              radius: 20,
              callback: () {
                launch("tel://" + AppDefine.SUPPORT_CALL1);
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
            child: MMArrowBtn(
              txt: "call".tr + ": " + AppDefine.SUPPORT_CALL2,
              icon: null,
              width: getW(context),
              height: getHP(context, MyTheme.btnHpa),
              radius: 20,
              callback: () {
                launch("tel://" + AppDefine.SUPPORT_CALL2);
              },
            ),
          ),
          Txt(
              txt: "or".tr,
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: false),
          Padding(
            padding: const EdgeInsets.all(20),
            child: MMArrowBtn(
              txt: "send_message".tr,
              icon: null,
              width: getW(context),
              height: getHP(context, MyTheme.btnHpa),
              radius: 20,
              callback: () async {
                Get.to(
                  () => ResolutionScreen(),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
