import 'package:aitl/config/server/APIYoutubeCfg.dart';
import 'package:aitl/config/server/ServerUrls.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/dashboard/more/help/support_center_page.dart';
import 'package:aitl/view/dashboard/more/help/tutorials/app_tut2_page.dart';
import 'package:aitl/view/widgets/views/SoundLngBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/rx/AudioController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../more_page.dart';

class HelpPage extends StatefulWidget {
  HelpPage({Key key}) : super(key: key);
  @override
  State createState() => _HelpPageState();
}

//const FIND_WORK_RELOAD_ON_APP_TUT2 = "Guide to becoming a tasker";

class _HelpPageState extends State<HelpPage> with Mixin, UIHelper {
  final audioController = Get.put(AudioController());
  List<Map<String, dynamic>> listMore;

  onTaskerVideoClicked() {
    openUrl(context, APIYoutubeCfg.HELP_TASKER_YOUTUBE_URL);
  }

  onPosterVideoClicked() {
    openUrl(context, APIYoutubeCfg.HELP_POSTER_YOUTUBE_URL);
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    listMore = null;
    super.dispose();
  }

  appInit() {
    try {} catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    listMore = [
      {"title": "support_centre".tr, "route": () => SupportCenterPage()},
      {
        "title": "terms_and_conditions".tr,
        "route": () =>
            WebScreen(url: ServerUrls.TC_URL, title: "terms_and_conditions".tr)
      },
      {
        "title": "privacy_screen_title".tr,
        "route": () => WebScreen(
            url: ServerUrls.PRIVACY_URL, title: "privacy_screen_title".tr)
      },
      {"title": "guide_around_shohokari".tr, "route": null},
      {"title": "guide_become_tasker".tr, "route": () => AppTut2Page()},
      {"title": "tasker_tutorial".tr, "callback": () => onTaskerVideoClicked()},
      {"title": "poster_tutorial".tr, "callback": () => onPosterVideoClicked()},
    ];
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          elevation: MyTheme.appbarElevation,
          title: UIHelper().drawAppbarTitle(title: 'help'.tr),
          actions: drawSoundLngBox(this.runtimeType, audioController, []),
          centerTitle: false,
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Container(
        child: ListView.builder(
          itemCount: listMore.length,
          itemBuilder: (context, index) {
            Map<String, dynamic> mapMore = listMore[index];
            return GestureDetector(
              onTap: () async {
                if (mapMore['route'] != null) {
                  Get.to(mapMore['route']);
                } else if (mapMore['callback'] != null) {
                  Function.apply(mapMore['callback'], []);
                } else {
                  Get.back(result: {'moreEvent': eMoreEvent.app_tut1});
                }
              },
              child: Container(
                color: Colors.transparent,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child: Row(
                        children: [
                          Expanded(
                            child: Txt(
                                txt: mapMore['title'].toString(),
                                txtColor: MyTheme.gray5Color,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.start,
                                isBold: false),
                          ),
                          Icon(
                            Icons.arrow_forward_ios,
                            color: MyTheme.gray3Color,
                            size: 20,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 30),
                    drawLine(),
                    SizedBox(height: 10),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
