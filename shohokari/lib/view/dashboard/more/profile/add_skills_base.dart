import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';

abstract class BaseAddSkillsStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  final textField = TextEditingController();
  List<String> listSkills = [];

  wsAddSkill(bool isRemove);

  drawAddSkillView(String title, String hint) {
    return Container(
      child: Padding(
        padding:
            const EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Txt(
                txt: hint,
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: false),
            SizedBox(height: 10),
            Row(
              children: [
                Expanded(
                  child: TextField(
                    controller: textField,
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: MyTheme.gray5Color,
                      fontSize: getTxtSize(
                          context: context, txtSize: MyTheme.txtSize),
                      height: MyTheme.txtLineSpace,
                    ),
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(
                          bottom: 0, top: 0, left: 10, right: 10),
                      counter: Offstage(),
                      enabledBorder: const OutlineInputBorder(
                        borderSide:
                            const BorderSide(color: Colors.grey, width: 0.0),
                        borderRadius: const BorderRadius.all(
                          const Radius.circular(10.0),
                        ),
                      ),
                      focusedBorder: const OutlineInputBorder(
                        borderSide:
                            const BorderSide(color: Colors.black, width: 0.0),
                        borderRadius: const BorderRadius.all(
                          const Radius.circular(10.0),
                        ),
                      ),
                      border: OutlineInputBorder(),
                      hintText: '',
                      hintStyle: TextStyle(color: Colors.grey),
                    ),
                  ),
                ),
                TextButton(
                    onPressed: () {
                      FocusScope.of(context).requestFocus(new FocusNode());
                      wsAddSkill(false);
                    },
                    child: Txt(
                      txt: "add".tr,
                      txtColor: MyTheme.gray5Color,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false,
                    ))
              ],
            )
          ],
        ),
      ),
    );
  }

  drawSkillList() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
      child: ListView.builder(
        shrinkWrap: true,
        primary: false,
        reverse: true,
        itemCount: listSkills.length,
        itemBuilder: (context, i) {
          return ListTile(
            title: Txt(
                txt: listSkills[i],
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.start,
                isBold: false),
            trailing: IconButton(
              icon: Icon(
                Icons.remove_circle_outline_rounded,
                color: Colors.red,
              ),
              onPressed: () {
                listSkills.remove(listSkills[i]);
                wsAddSkill(true);
              },
            ),
          );
        },
      ),
    );
  }
}
