import 'package:aitl/config/server/APINotiCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/audio/audio_mgr.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/noti/NotiAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/noti/NotiModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/more/db/db_base.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';

class NotiPage extends StatefulWidget {
  final int notificationId;
  final String title, body, webUrl;
  const NotiPage(
      {Key key, this.notificationId, this.title, this.body, this.webUrl})
      : super(key: key);
  @override
  State createState() => _NotiPageState();
}

class _NotiPageState extends BaseDashboardMoreStatefull<NotiPage>
    with APIStateListener, StateListener {
  List<NotiModel> listNoti = [];

//  **************  app states start

  StateProvider _stateProvider;
  @override
  void onStateChanged(state, data) async {
    try {
      if (state == ObserverState.STATE_AUDIO_START &&
          data == this.runtimeType) {
        await audioController.getAudio();
        if (audioController.isAudio.value)
          AudioMgr().play("more_notifications");
      } else if (state == ObserverState.STATE_AUDIO_STOP) {
        AudioMgr().stop();
      }
    } catch (e) {}
  }

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  Future<void> refreshData() async {
    try {
      if (mounted) {
        setState(() {
          isLoading = true;
        });
        await APIViewModel().req<NotiAPIModel>(
            context: context,
            apiState: APIState(APIType.get_noti, this.runtimeType, null),
            url: APINotiCfg.NOTI_GET_URL
                .replaceAll("#UserId#", userData.userModel.id.toString()),
            //param: {"UserId": userData.userModel.id},
            reqType: ReqType.Get);
      }
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    } catch (e) {}
  }

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.get_noti &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            listNoti = (model as NotiAPIModel).responseData.notifications;
            setState(() {});
          }
        }
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    try {
      myTaskController.dispose();
    } catch (e) {}
    try {
      AudioMgr().stop2();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
      StateProvider().notify(ObserverState.STATE_AUDIO_START, this.runtimeType);
    } catch (e) {}
    try {
      await PrefMgr.shared.setPrefInt("unreadNotificationCount",
          userData.userModel.unreadNotificationCount);
    } catch (e) {}
    refreshData();
    /*try {
      await APIViewModel().req<NotiAPIModel>(
          context: context,
          url: APINotiCfg.NOTI_PUT_URL,
          param: {"UserId": userData.userModel.id},
          reqType: ReqType.Put,
          callback: (model) {});
    } catch (e) {}*/
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          title: UIHelper().drawAppbarTitle(title: 'notifications'.tr),
          centerTitle: false,
        ),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: RefreshIndicator(
        color: Colors.white,
        backgroundColor: MyTheme.brandColor,
        onRefresh: refreshData,
        child: SingleChildScrollView(
          primary: true,
          child: drawNotiView(listNoti, true),
        ),
      ),
    );
  }
}
