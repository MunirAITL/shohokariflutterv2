import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:flutter/material.dart';

enum enumNotiSettings {
  Transactional,
  TaskUpdates,
  TaskReminders,
  AppAlerts,
  TaskRecommendations,
  HelpfulInfo,
  UpdateNewsLettters,
}

abstract class NotiSettingsStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> {}
