import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/server/APIPaymentCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/payment/methods/PaymentMethodAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/payment/methods/PaymentMethodsAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/payment/methods/PaymentMethodsModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/btn/BottomBtn.dart';
import 'package:aitl/view/widgets/input/InputBox.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../payment_methods/receive_payment/receive_payment_base.dart';

class AddBKashPage extends StatefulWidget {
  const AddBKashPage({Key key}) : super(key: key);

  @override
  _AddBKashPageState createState() => _AddBKashPageState();
}

class _AddBKashPageState extends BaseReceivePaymentStatefull<AddBKashPage> {
  final accNo = TextEditingController();
  final focusCode = FocusNode();

  List<PaymentMethodsModel> listPaymentMethodsModel;
  PaymentMethodsModel paymentMethodBKash;

  //  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  wsAddBKashAccount() async {
    try {
      if (accNo.text.isEmpty) {
        showToast(
          context: context,
          msg: 'add_bank_account_error_account_length_gb'.tr,
        );
      } else {
        if (paymentMethodBKash != null) {
          APIViewModel().req<PaymentMethodAPIModel>(
              context: context,
              url: APIPaymentCfg.PAYMENT_METHODS_PUT_URL,
              param: {
                "AccountName": TaskStatusCfg.TASK_PAYMENT_METHOD_BIKASH,
                "AccountNumber": accNo.text.trim(),
                "CVC": "",
                "CardName": "",
                "CardNumber": "",
                "CreationDate": DateTime.now().toString(),
                "ExpiryDate": "",
                "ExpiryMonth": "",
                "Id": paymentMethodBKash.id,
                "PaymentMethodType": TaskStatusCfg.TASK_PAYMENT_METHOD_BIKASH,
                "Status": TaskStatusCfg.TASK_STATUS_ACTIVE,
                "UpdatedDate": DateTime.now().toString(),
                "UserId": userData.userModel.id,
              },
              reqType: ReqType.Put,
              callback: (model) {
                if (model != null && mounted) {
                  if (model.success) {
                    PrefMgr.shared.setBikashAccount(TaskStatusCfg()
                        .getSatusCode(TaskStatusCfg.STATUS_UNVERIFIED));
                    showToast(
                        context: context,
                        msg: 'bikash_updated_msg'.tr,
                        which: 1);
                    Get.back();
                  }
                }
              });
        } else {
          APIViewModel().req<PaymentMethodAPIModel>(
              context: context,
              url: APIPaymentCfg.PAYMENT_METHODS_POST_URL,
              param: {
                "AccountName": TaskStatusCfg.TASK_PAYMENT_METHOD_BIKASH,
                "AccountNumber": accNo.text.trim(),
                "CVC": "",
                "CardName": "",
                "CardNumber": "",
                "CreationDate": DateTime.now().toString(),
                "ExpiryDate": "",
                "ExpiryMonth": "",
                "PaymentMethodType": TaskStatusCfg.TASK_PAYMENT_METHOD_BIKASH,
                "Status": TaskStatusCfg.TASK_STATUS_ACTIVE,
                "UpdatedDate": DateTime.now().toString(),
                "UserId": userData.userModel.id,
              },
              reqType: ReqType.Post,
              callback: (model) {
                if (model != null && mounted) {
                  if (model.success) {
                    PrefMgr.shared.setBikashAccount(TaskStatusCfg()
                        .getSatusCode(TaskStatusCfg.STATUS_UNVERIFIED));
                    showToast(
                        context: context, msg: 'bikash_added_msg'.tr, which: 1);
                    Get.back();
                  }
                }
              });
        }
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    accNo.dispose();
    listPaymentMethodsModel = null;
    try {
      myTaskController.dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      APIViewModel().req<PaymentMethodsAPIModel>(
          context: context,
          url: APIPaymentCfg.PAYMENT_METHODS_GET_URL,
          param: {"UserId": userData.userModel.id},
          reqType: ReqType.Get,
          callback: (model) {
            if (model != null && mounted) {
              if (model.success) {
                listPaymentMethodsModel = model.responseData.paymentMethods;
                for (var model2 in listPaymentMethodsModel) {
                  if (model2.paymentMethodType ==
                      TaskStatusCfg.TASK_PAYMENT_METHOD_BIKASH) {
                    paymentMethodBKash = model2;
                    accNo.text = paymentMethodBKash.accountNumber.trim();
                  }
                }
                setState(() {});
              }
            }
          });
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          title: UIHelper()
              .drawAppbarTitle(title: 'mobile_payment_screen_title_bikash'.tr),
          centerTitle: false,
        ),
        bottomNavigationBar: drawBottomBtn(
            context: context,
            text: "add".tr,
            callback: () async {
              if (accNo.text.trim().length ==
                  TaskStatusCfg.BIKASH_NUMBER_LENGTH) {
                wsAddBKashAccount();
              } else {
                showToast(
                    context: context, msg: 'mobile_payment_error_number'.tr);
              }
            }),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  @override
  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              InputBox(
                context: context,
                ctrl: accNo,
                lableTxt: "account_number".tr,
                labelColor: MyTheme.gray4Color,
                align: TextAlign.start,
                kbType: TextInputType.text,
                inputAction: TextInputAction.done,
                focusNode: focusCode,
                len: TaskStatusCfg.BIKASH_NUMBER_LENGTH,
              ),
              SizedBox(height: 50),
            ],
          ),
        ),
      ),
    );
  }
}
