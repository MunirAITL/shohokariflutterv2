import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/dashboard/more/payment/payment_methods/make_payment/add_promo_card_page.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class MakePaymentPage extends StatelessWidget with UIHelper {
  MakePaymentPage({Key key}) : super(key: key);

  List<String> listItem = [];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  drawLayout() {
    listItem = ['payment_setting_add_promotion_code'.tr.toUpperCase()];
    return Container(
      color: Colors.white,
      child: ListView.builder(
        itemCount: listItem.length,
        itemBuilder: (BuildContext context, int index) {
          final title = listItem[index];
          Widget wid = SizedBox();
          switch (index) {
            case 0:
              wid = Icon(
                Icons.add_circle_outline_rounded,
                color: MyTheme.blueColor,
                size: 25,
              );
              break;

            default:
          }
          return Padding(
            padding: const EdgeInsets.all(10),
            child: Container(
              color: Colors.transparent,
              child: ListTile(
                  onTap: () {
                    switch (index) {
                      case 0:
                        Get.to(() => AddPromoCardPage());
                        break;
                      case 1:
                        break;
                      default:
                    }
                  },
                  leading: wid,
                  minLeadingWidth: 0,
                  title: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(height: 12),
                      Txt(
                          txt: title,
                          txtColor: MyTheme.hotdipPink,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: false),
                      SizedBox(height: 20),
                      drawLine(),
                    ],
                  )),
            ),
          );
        },
      ),
    );
  }
}
