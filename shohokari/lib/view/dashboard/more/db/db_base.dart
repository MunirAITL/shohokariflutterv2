import 'package:aitl/config/app/status/NotiStatusCfg.dart';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/server/APIMyTasksCfg.dart';
import 'package:aitl/config/server/ServerUrls.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/noti/NotiModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingSummaryDataModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/GetTaskAPIModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/TaskSummaryUserDataModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/dashboard/messages/chat_page.dart';
import 'package:aitl/view/dashboard/messages/comments/offers_comments_page.dart';
import 'package:aitl/view/dashboard/more/noti/noti_page.dart';
import 'package:aitl/view/dashboard/more/noti/noti_task_alert.dart';
import 'package:aitl/view/dashboard/mytasks/taskdetails_page.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/switchview/ToggleSwitch.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/rx/MyTaskController.dart';
import 'package:aitl/view_model/rx/ProfileController.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class BaseDashboardMoreStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  final myTaskController = Get.put(MyTaskController());
  final profileController = Get.put(ProfileController());
  int posterSwitchValue = 0;
  bool isTasker = true;
  bool isLoading = true;

  drawUserSwitchView() {
    return Center(
      child: Padding(
        padding: const EdgeInsets.only(top: 20),
        child: ToggleSwitch(
          isBorderColor: true,
          minWidth: getWP(context, 35),
          minHeight: getHP(context, MyTheme.switchBtnHpa),
          initialLabelIndex: posterSwitchValue,
          cornerRadius: 50.0,
          fontSize: isLng(AppDefine.GEO_CODE) ? 14 : 17,
          activeBgColor: MyTheme.brandColor,
          activeFgColor: Colors.white,
          inactiveBgColor: Colors.white,
          inactiveFgColor: MyTheme.brandColor,
          labels: ['as_tasker'.tr, 'as_poster'.tr],
          //icons: [FontAwesomeIcons.mars, FontAwesomeIcons.venus],
          onToggle: (index) {
            posterSwitchValue = index;
            isTasker = (posterSwitchValue == 0) ? true : false;
            setState(() {});
          },
        ),
      ),
    );
  }

  drawStats(TaskSummaryUserDataModel taskSummaryUserData,
      List<UserRatingSummaryDataModel> listUserRatingSummary) {
    int bidOnVal = 0;
    int openOfferVal = 0;
    int assignedVal = 0;
    int overDueVal = 0;
    int awaitingPayVal = 0;
    int taskCompletedVal = 0;
    if (taskSummaryUserData != null) {
      bidOnVal = taskSummaryUserData.taskBidOnCount;
      openOfferVal = taskSummaryUserData.posterTaskOpenForBidCount;
      assignedVal = taskSummaryUserData.taskAssignedCount;
      overDueVal = taskSummaryUserData.taskOverDueCount;
      awaitingPayVal = taskSummaryUserData.taskAwaitingPaymentCount;
      taskCompletedVal = taskSummaryUserData.taskCompletedCount;
    }

    return Container(
      child: Column(
        children: [
          Padding(
            padding:
                const EdgeInsets.only(top: 10, bottom: 10, left: 20, right: 20),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                _drawStatsBox(
                    MyTheme.airGreenColor,
                    (isTasker)
                        ? "dashboard_column_bid_on_label".tr
                        : "dashboard_column_open_for_offers_label".tr,
                    (isTasker) ? bidOnVal : openOfferVal),
                _drawStatsBox(MyTheme.hotdipPink, "assigned".tr, assignedVal),
                _drawStatsBox(
                    MyTheme.airOrange, "dashboard_tab_overdue".tr, overDueVal),
                _drawStatsBox(MyTheme.airOrange,
                    "dashboard_tab_awaiting_payments".tr, awaitingPayVal),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
                left: getWP(context, 3), right: getWP(context, 3)),
            child: Container(
              child: IntrinsicHeight(
                child: Row(
                  children: [
                    Container(
                      width: getWP(context, 40),
                      color: Colors.white,
                      child: Padding(
                        padding: const EdgeInsets.all(20),
                        child: Column(
                          children: [
                            Txt(
                                txt: taskCompletedVal.toString(),
                                txtColor: MyTheme.gray5Color,
                                txtSize: MyTheme.txtSize + 2,
                                txtAlign: TextAlign.center,
                                isBold: false),
                            Txt(
                                txt: "completed".tr,
                                txtColor: MyTheme.gray4Color,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.center,
                                isBold: false),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(width: getWP(context, 1)),
                    Expanded(
                      child: Container(
                        color: Colors.white,
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 10, right: 10, top: 20, bottom: 20),
                          child:
                              drawUserRatingSummaryView(listUserRatingSummary),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _drawStatsBox(Color txt1Color, String title, int val) {
    return Flexible(
      child: Column(
        children: [
          Txt(
              txt: val.toString(),
              txtColor: txt1Color,
              txtSize: MyTheme.txtSize + .5,
              txtAlign: TextAlign.center,
              isBold: false),
          Txt(
              txt: title,
              txtColor: MyTheme.gray4Color,
              txtSize: MyTheme.txtSize - .2,
              txtAlign: TextAlign.center,
              isBold: false),
        ],
      ),
    );
  }

  drawUserRatingSummaryView(
      List<UserRatingSummaryDataModel> listUserRatingSummary) {
    if (listUserRatingSummary == null) return SizedBox();
    int rate = 0;
    int completionRate = 0;
    int reviews = 0;
    if (!isTasker) {
      rate = profileController.aveargeRatingAsPoster.value;
      completionRate = profileController.completionRateAsPoster.value;
      reviews = profileController.countAsPoster.value;
    } else {
      if (listUserRatingSummary.length > 0) {
        rate = listUserRatingSummary[0].avgRating.toInt();
        completionRate = listUserRatingSummary[0].taskerCompletionRate;
        reviews = listUserRatingSummary[0].ratingCount;
      }
    }

    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          UIHelper().getStarRatingView(
            rate: rate,
            reviews: reviews,
            isRow: false,
            starColor: Colors.black,
            txtColor: MyTheme.gray4Color,
          ),
          //SizedBox(height: 5),
          UIHelper().getCompletionText(
              pa: completionRate,
              txtColor: MyTheme.gray4Color,
              callbackInfo: () {
                if (isTasker) {
                  showAlert(
                      context: context,
                      msg: "dialog_completion_rate_message_runner".tr);
                } else {
                  showAlert(
                      context: context,
                      msg: "dialog_completion_rate_message_sender".tr);
                }
              }),
        ],
      ),
    );
  }

  drawNotiView(List<NotiModel> listNoti, bool isNotiTab) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Container(
        color: Colors.white,
        width: getW(context),
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Padding(
            padding: EdgeInsets.only(
                left: 10,
                right: 10,
                top: (!isNotiTab) ? 10 : 0,
                bottom: (!isNotiTab) ? 10 : 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                (!isNotiTab)
                    ? Padding(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: Txt(
                            txt: "notifications".tr,
                            txtColor: MyTheme.gray4Color,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      )
                    : SizedBox(),
                (listNoti.length > 0)
                    ? ListView.builder(
                        addAutomaticKeepAlives: true,
                        cacheExtent: AppConfig.page_limit.toDouble(),
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        primary: false,
                        itemCount: (!isNotiTab && listNoti.length > 10)
                            ? 10
                            : listNoti.length,
                        itemBuilder: (BuildContext context, int index) {
                          return drawNotiItem(
                              listNoti[index], isNotiTab, index);
                        },
                      )
                    : (!isLoading)
                        ? Column(
                            children: [
                              (isNotiTab)
                                  ? Container(
                                      width: getWP(context, 60),
                                      height: getWP(context, 40),
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                          image: AssetImage(
                                              "assets/images/nf/nf_noti.png"),
                                          //fit: BoxFit.fitWidth,
                                        ),
                                      ),
                                    )
                                  : SizedBox(),
                              Container(
                                  child: Txt(
                                      txt: "noti_nf".tr,
                                      txtColor: MyTheme.gray4Color,
                                      txtSize: MyTheme.txtSize,
                                      txtAlign: TextAlign.start,
                                      isBold: false)),
                            ],
                          )
                        : SizedBox(),
                (!isNotiTab && listNoti.length > 10)
                    ? _go2Notification()
                    : SizedBox()
              ],
            ),
          ),
        ),
      ),
    );
  }

  drawNotiItem(NotiModel notiModel, bool isNotiTab, int index) {
    try {
      final doubletik =
          (notiModel.isRead) ? "check2_icon.png" : "check1_icon.png";

      final map = _getNotiEvents(notiModel);
      var msg = '';
      var msg2 = '';
      if (!isNotiTab) {
        msg = notiModel.initiatorDisplayName + 'commented_on'.tr;
        msg2 = " " + notiModel.message;
      } else {
        msg = map['description'];
        msg = msg.trim();
        if (map['isTaskDetails'] as bool)
          msg = "task_posted_matching_skills".tr + "[" + msg + "]";
        msg2 = " " + notiModel.message;
      }

      return GestureDetector(
        onTap: () async {
          //  munir->dashboardnotifications
          if (map['callback'] != null) Function.apply(map['callback'], []);
        },
        child: Padding(
          padding: const EdgeInsets.only(top: 10, bottom: 10),
          child: Container(
            //height: getHP(context, 25),
            color: Colors.transparent,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircleAvatar(
                  radius: (!isNotiTab) ? 22 : 25,
                  backgroundColor: Colors.transparent,
                  backgroundImage: new CachedNetworkImageProvider(
                    MyNetworkImage.checkUrl((notiModel != null)
                        ? notiModel.initiatorImageUrl
                        : ServerUrls.MISSING_IMG),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        //SizedBox(height: 20),
                        Text.rich(
                          TextSpan(
                            children: [
                              TextSpan(
                                  text: msg,
                                  style: TextStyle(
                                      color: MyTheme.gray5Color, fontSize: 17)),
                              TextSpan(
                                text: msg2,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: MyTheme.gray5Color,
                                    fontSize: 17),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 5),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Txt(
                                txt: DateFun.getTimeAgoTxt(
                                    notiModel.publishDateTime),
                                txtColor: MyTheme.gray4Color,
                                txtSize: MyTheme.txtSize - .5,
                                txtAlign: TextAlign.start,
                                isBold: false),
                            //SizedBox(width: 10),
                            Image.asset(
                              "assets/images/icons/" + doubletik,
                              width: 15,
                              height: 15,
                              color: (notiModel.isRead)
                                  ? Colors.green
                                  : Colors.grey,
                            )
                          ],
                        ),
                        //SizedBox(height: 20),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    } catch (e) {
      myLog(e.toString());
    }
    return SizedBox();
  }

  _go2Notification() {
    return Container(
      alignment: Alignment.centerRight,
      child: TextButton(
          onPressed: () {
            Get.to(() => NotiPage());
          },
          child: Txt(
            txt: "more".tr + '...',
            txtColor: MyTheme.brandColor,
            txtSize: MyTheme.txtSize - .2,
            txtAlign: TextAlign.start,
            isBold: false,
          )),
    );
  }

  _getNotiEvents(NotiModel notiModel) {
    var description = '';
    var callback;
    bool isTaskDetails = false;
    final name = notiModel.initiatorDisplayName;
    if (notiModel.userId != userData.userModel.id &&
        notiModel.entityName == NotiStatusCfg.NOTIFICATION_NAME_Task) {
      description =
          "push_notification_task_alert".tr.replaceAll("#name#", name);
      callback = () => go2TaskDetailsPage(notiModel, description, true);
    } else if (notiModel.entityName ==
        NotiStatusCfg.NOTIFICATION_NAME_TaskBidding) {
      description =
          "push_notification_make_offer".tr.replaceAll("#name#", name);
      callback = () => go2CommentsPage(notiModel, description);
    } else if (notiModel.entityName ==
            NotiStatusCfg.NOTIFICATION_NAME_TaskBiddingComment ||
        notiModel.entityName ==
            NotiStatusCfg.NOTIFICATION_NAME_TaskBiddingAdminComment) {
      description =
          "push_notification_task_comment".tr.replaceAll("#name#", name);
      switch (notiModel.notificationEventId) {
        case 1011:
          description =
              "push_notification_private_message".tr.replaceAll("#name#", name);
          callback = () =>
              go2ChatPage(taskId: notiModel.entityId, description: description);
          break;
        case 1012:
          description =
              "push_notification_offer_message".tr.replaceAll("#name#", name);
          callback = () => go2CommentsPage(notiModel, description);
          break;
        default:
          callback = () => go2ChatPage(
              taskId: notiModel.entityId,
              isPublicChat: true,
              description: description); //  Public Chat Page
      }
    } else if (notiModel.entityName ==
        NotiStatusCfg.NOTIFICATION_NAME_TaskBiddingRequestPayment) {
      description =
          "push_notification_private_message".tr.replaceAll("#name#", name);
      callback = () =>
          go2ChatPage(taskId: notiModel.entityId, description: description);
    } else if (notiModel.entityName ==
        NotiStatusCfg.NOTIFICATION_NAME_TaskBiddingAccepted) {
      description =
          "push_notification_accept_offer".tr.replaceAll("#name#", name);
      callback = () => go2CommentsPage(notiModel, description);
    } else if (notiModel.entityName ==
        NotiStatusCfg.NOTIFICATION_NAME_TaskPaymentConfirmation) {
      description = "push_notification_payment_confirmation"
          .tr
          .replaceAll("#name#", name);
      callback = () => go2CommentsPage(notiModel, description);
    } else if (notiModel.entityName ==
        NotiStatusCfg.NOTIFICATION_NAME_UserRating) {
      description =
          "push_notification_user_review".tr.replaceAll("#name#", name);
      callback = () => go2TaskDetailsPage(notiModel, description, false);
    } else if (notiModel.entityName ==
        NotiStatusCfg.NOTIFICATION_NAME_Resolution) {
      callback = () =>
          go2ChatPage(taskId: notiModel.entityId, description: description);
    } else {
      isTaskDetails = true;
      //callback = () => go2NotiTab(notiModel);
      callback = () => go2TaskDetailsPage(notiModel, description, true);
    }
    return {
      'description': description,
      'callback': callback,
      'isTaskDetails': isTaskDetails,
    };
  }

  //  go2
  go2TaskDetailsPage(NotiModel notiModel, description, bool isTaskid) async {
    try {
      await APIViewModel().req<GetTaskAPIModel>(
        context: context,
        url: APIMyTasksCfg.GET_TASK_URL.replaceAll(
            "#taskId#",
            (isTaskid)
                ? notiModel.entityId.toString()
                : notiModel.description.toString()),
        reqType: ReqType.Get,
        callback: (model) async {
          if (model != null && mounted) {
            if (model.success) {
              final taskModel = model.responseData.task;
              if (taskModel != null) {
                await myTaskController.setTaskModel(taskModel);
                Get.to(() => TaskDetailsPage(
                      taskModel: model.responseData.task,
                      description: description,
                      title: notiModel.message,
                      body: notiModel.description,
                    )).then((value) {
                  myTaskController.setTaskModel(null);
                });
              }
            }
          } else {
            Get.to(() =>
                NofiTaskAlert(notiModel: notiModel, description: description));
          }
        },
      );
    } catch (e) {}
  }

  go2CommentsPage(NotiModel notiModel, description) async {
    try {
      await APIViewModel().req<TaskBiddingAPIModel>(
        context: context,
        url: APIMyTasksCfg.GET_TASKBIDDING_URL
            .replaceAll("#taskBiddingId#", notiModel.entityId.toString()),
        reqType: ReqType.Get,
        callback: (model2) async {
          if (model2 != null && mounted) {
            if (model2.success) {
              await APIViewModel().req<GetTaskAPIModel>(
                context: context,
                url: APIMyTasksCfg.GET_TASK_URL.replaceAll("#taskId#",
                    model2.responseData.taskBidding.taskId.toString()),
                reqType: ReqType.Get,
                callback: (model) async {
                  if (model != null && mounted) {
                    if (model.success) {
                      final taskModel = model.responseData.task;
                      if (taskModel != null) {
                        await myTaskController.setTaskModel(taskModel);
                        Get.to(() => OffersCommentsPage(
                              taskBiddingsModel:
                                  model2.responseData.taskBidding,
                              description: description,
                              title: notiModel.message,
                              body: notiModel.description,
                            )).then((value) {
                          myTaskController.setTaskModel(null);
                        });
                      }
                    }
                  }
                },
              );
            }
          }
        },
      );
    } catch (e) {}
  }

  go2ChatPage({taskId, bool isPublicChat, String description}) async {
    try {
      await APIViewModel().req<GetTaskAPIModel>(
        context: context,
        url: APIMyTasksCfg.GET_TASK_URL
            .replaceAll("#taskId#", taskId.toString()),
        reqType: ReqType.Get,
        callback: (model) async {
          if (model != null && mounted) {
            if (model.success) {
              final taskModel = model.responseData.task;
              if (taskModel != null) {
                await myTaskController.setTaskModel(taskModel);
                Get.to(() => ChatPage(
                      isPublicChat: isPublicChat,
                      description: description,
                    )).then((value) {
                  myTaskController.setTaskModel(null);
                });
              }
            }
          }
        },
      );
    } catch (e) {}
  }

  go2NotiTab(NotiModel notiModel) async {
    Get.to(() => NotiPage(
          notificationId: notiModel.id,
          title: notiModel.message,
          body: notiModel.description,
          webUrl: notiModel.webUrl,
        ));
  }

  go2TaskNotiWeb(NotiModel notiModel, String description) async {
    Get.to(() => WebScreen(url: notiModel.webUrl, title: notiModel.message));
  }
}
