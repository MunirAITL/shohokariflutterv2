import 'package:aitl/config/server/APITaskAlertCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/audio/audio_mgr.dart';
import 'package:aitl/view/widgets/dialog/ConfirmDialog.dart';
import 'package:aitl_pkg/classes/Common.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/task_alert/TaskAlertAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/task_alert/TaskAlertsModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/btn/BottomBtn.dart';
import 'package:aitl/view/widgets/dropdown/DropDownWidget.dart';
import 'package:aitl/view/widgets/google/GPlacesView.dart';
import 'package:aitl/view/widgets/switchview/SwitchView.dart';
import 'package:aitl/view/widgets/input/InputBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_webservice/geocoding.dart';

import 'task_alert_base.dart';

class TaskAlertAmendPage extends StatefulWidget {
  final TaskAlertKeywordsModel taskAlertKeywordsModel;
  const TaskAlertAmendPage({Key key, this.taskAlertKeywordsModel})
      : super(key: key);
  @override
  State createState() => _TaskAlertAmendPageState();
}

class _TaskAlertAmendPageState
    extends BaseTaskAlertKWStatefull<TaskAlertAmendPage>
    with APIStateListener, StateListener {
  final kw = TextEditingController();

  var listDropdown = [
    "5km",
    "10km",
    "15km",
    "20km",
    "25km",
    "30km",
  ];

  final listDistance = [5, 10, 15, 20, 25, 30];

  var dropdownVal = "";
  bool isInPerson = true;
  int switchIndex = 0;

  String taskAddress = "search".tr;
  Location taskCord;

  //  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  StateProvider _stateProvider;
  @override
  void onStateChanged(state, data) async {
    try {
      if (state == ObserverState.STATE_AUDIO_START &&
          data == this.runtimeType) {
        await audioController.getAudio();
        if (audioController.isAudio.value)
          AudioMgr().play("add_task_alert_setting");
      } else if (state == ObserverState.STATE_AUDIO_STOP) {
        AudioMgr().stop();
      }
    } catch (e) {}
  }

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.task_alert_post &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            setState(() {
              Get.back();
            });
          }
        }
      } else if (apiState.type == APIType.task_alert_put &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            Get.back();
          }
        }
      } else if (apiState.type == APIType.task_alert_del &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            setState(() {
              Get.back();
            });
          }
        }
      }
    } catch (e) {}
  }

  @override
  drawItem(item) {}

  validate() {
    if (kw.text.trim().length < 3) {
      showToast(context: context, msg: 'kw_long'.tr);
      return false;
    }
    if (isInPerson && (taskAddress == "" || taskAddress == "search".tr)) {
      showToast(context: context, msg: "pick_loc".tr);
      return false;
    }
    return true;
  }

  wsTaskAlertAdd() async {
    try {
      var index = 0;
      double lat = 0;
      double lng = 0;
      try {
        index = Common.findIndexFromList(listDropdown, dropdownVal);
        lat = taskCord.lat;
        lng = taskCord.lng;
      } catch (e) {}
      await APIViewModel().req<TaskAlertAPIModel>(
          context: context,
          apiState: APIState(APIType.task_alert_post, this.runtimeType, null),
          url: APITaskAlertCfg.TASK_ALERT_POST_URL,
          param: {
            "Distance": listDistance[index],
            "IsOnline": !isInPerson,
            "Keyword": kw.text.trim(),
            "Latitude": (isInPerson) ? lat : 0,
            "Location": (isInPerson)
                ? taskAddress != 'search'.tr
                    ? taskAddress
                    : ''
                : '',
            "Longitude": (isInPerson) ? lng : 0,
            "UserId": userData.userModel.id,
          },
          reqType: ReqType.Post,
          callback: (model) {});
    } catch (e) {}
  }

  wsTaskAlertUpdate() async {
    try {
      var index = 0;
      double lat = 0;
      double lng = 0;
      try {
        index = Common.findIndexFromList(listDropdown, dropdownVal);
        lat = taskCord.lat;
        lng = taskCord.lng;
      } catch (e) {}
      await APIViewModel().req<TaskAlertAPIModel>(
          context: context,
          apiState: APIState(APIType.task_alert_put, this.runtimeType, null),
          url: APITaskAlertCfg.TASK_ALERT_PUT_URL,
          param: {
            "Distance": listDistance[index],
            "Id": widget.taskAlertKeywordsModel.id,
            "IsOnline": !isInPerson,
            "Keyword": kw.text.trim(),
            "Latitude": (isInPerson) ? lat : 0,
            "Location": (isInPerson)
                ? taskAddress != 'search'.tr
                    ? taskAddress
                    : ''
                : '',
            "Longitude": (isInPerson) ? lng : 0,
            "UserId": userData.userModel.id,
          },
          reqType: ReqType.Put,
          callback: (model) {});
    } catch (e) {}
  }

  onDelAlertClicked(int taskId) async {
    try {
      await APIViewModel().req<TaskAlertAPIModel>(
          context: context,
          apiState: APIState(APIType.task_alert_del, this.runtimeType, null),
          url: APITaskAlertCfg.TASK_ALERT_DEL_URL.replaceAll(
              "#alertId#", widget.taskAlertKeywordsModel.id.toString()),
          reqType: ReqType.Delete,
          callback: (model) {});
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    kw.dispose();
    try {
      AudioMgr().stop2();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
      StateProvider().notify(ObserverState.STATE_AUDIO_START, this.runtimeType);
    } catch (e) {}

    try {
      final model = widget.taskAlertKeywordsModel;
      if (model != null) {
        //  edit
        var index = 0;
        try {
          index = Common.findIndexFromList(listDistance, model.distance);
        } catch (e) {}
        dropdownVal = listDropdown[index];
        kw.text = model.keyword;
        isInPerson = !model.isOnline;
        taskAddress = model.location;
        taskCord = Location(lat: model.latitude, lng: model.longitude);
      } else {
        //  add
        dropdownVal = listDropdown[listDropdown.length - 1];
      }
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          title: UIHelper().drawAppbarTitle(
              title: (widget.taskAlertKeywordsModel == null)
                  ? "add_task_alert".tr
                  : "edit_task_alert".tr),
          centerTitle: false,
          actions: [
            widget.taskAlertKeywordsModel != null
                ? IconButton(
                    icon: Icon(Icons.delete),
                    onPressed: () {
                      confirmDialog(
                        context: context,
                        title: "dialog_task_alert_delete_title".tr,
                        msg: "dialog_task_alert_delete_message".tr,
                        callbackYes: () {
                          onDelAlertClicked(widget.taskAlertKeywordsModel.id);
                        },
                      );
                    },
                  )
                : SizedBox(),
          ],
        ),
        bottomNavigationBar: drawBottomBtn(
            context: context,
            text: (widget.taskAlertKeywordsModel == null)
                ? "add_alert".tr
                : "update_alert".tr,
            bgColor: Colors.white,
            callback: () async {
              if (widget.taskAlertKeywordsModel == null) {
                if (validate()) {
                  wsTaskAlertAdd();
                }
              } else {
                if (validate()) {
                  wsTaskAlertUpdate();
                }
              }
            }),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  @override
  drawLayout() {
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(height: 20),
            Center(
              child: SwitchView(
                onTxt: " " +
                    "browse_tasks_filter_task_type_task_with_location".tr +
                    " ",
                offTxt: " " + "list_item_task_remote_label".tr + " ",
                value: isInPerson,
                bgColorOn: MyTheme.brandColor,
                bgColorOff: Colors.white,
                txtColorOn: Colors.white,
                txtColorOff: MyTheme.brandColor,
                onChanged: (value) {
                  isInPerson = value;
                  setState(() {});
                  //callback((isSwitch) ? _companyName.text.trim() : '');
                },
              ),
            ),
            SizedBox(height: 20),
            Txt(
                txt: "kw_phrase".tr,
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: false),
            SizedBox(height: 20),
            InputBox(
              ctrl: kw,
              lableTxt: "enter_kw_phrase".tr,
              labelColor: MyTheme.gray4Color,
              isShowHint: true,
              kbType: TextInputType.text,
              len: 100,
            ),
            (isInPerson)
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(height: 10),
                      GPlacesView(
                        title: "browse_tasks_map_location_label".tr,
                        address: taskAddress,
                        txtColor: MyTheme.gray4Color,
                        bgColor: Colors.white,
                        isTxtBold: false,
                        titlePadding: 0,
                        callback: (String _address, Location _loc) {
                          //callback(address);
                          taskAddress = _address;
                          taskCord = _loc;
                          setState(() {});
                        },
                      ),
                      SizedBox(height: 15),
                      Txt(
                          txt: "distance".tr,
                          txtColor: MyTheme.gray4Color,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: false),
                      DropDownWidget(
                          list: listDropdown,
                          prefix: "within".tr,
                          dropdownValue: dropdownVal,
                          isExpanded: true,
                          txtColor: MyTheme.gray4Color,
                          callback: (val) {
                            dropdownVal = val;
                            setState(() {});
                          }),
                    ],
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }
}
