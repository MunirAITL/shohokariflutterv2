import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/data/model/dashboard/more/task_alert/TaskAlertsModel.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:flutter/material.dart';

abstract class BaseTaskAlertKWStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  drawItem(dynamic model);
  drawTaskAlertList(List<TaskAlertKeywordsModel> listTaskAlertKWModel) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: listTaskAlertKWModel.length,
        itemBuilder: (BuildContext context, int index) {
          return drawItem(listTaskAlertKWModel[index]);
        },
      ),
    );
  }
}
