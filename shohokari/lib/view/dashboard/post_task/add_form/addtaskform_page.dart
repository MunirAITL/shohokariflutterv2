import 'dart:convert';

import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/server/APIPostTaskCfg.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/posttask/allcat/ChildUserTaskCategorysModel.dart';
import 'package:aitl/data/model/dashboard/posttask/taskform/PostTaskFormAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/taskform/SubmitTaskFormAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/post_task/add_form/addtaskform_base.dart';
import 'package:aitl/view/dashboard/post_task/add_form/donetaskform_page.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_webservice/directions.dart';
import 'package:json_string/json_string.dart';

class AddTaskFormScreen extends StatefulWidget {
  final UserTaskItemResponseModelList model;
  const AddTaskFormScreen({Key key, @required this.model}) : super(key: key);
  @override
  State createState() => _AddTaskFormScreenState();
}

class _AddTaskFormScreenState
    extends BaseAddTaskFormStatefull<AddTaskFormScreen> with APIStateListener {
  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.get_taskform &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              listTaskItemFormModel = (model as PostTaskFormAPIModel)
                  .responseData
                  .userTaskItemForms;
              setState(() {});
            } catch (e) {}
          }
        }
      }
    } catch (e) {
      myLog(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }

    listTaskItemFormModel = null;
    //  page1
    optController.dispose();

    inputOthers = null;
    estBudget.dispose();

    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      controller.addListener(() {
        setState(() {
          currentPage = controller.page;
        });
      });
    } catch (e) {}
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    try {
      final date = new DateTime.now();
      dueDate = DateFun.getFormatedDate(
          format: 'dd-MMM-yyyy',
          mDate: DateTime(date.year, date.month, date.day));
      taskAddress = userData.userModel.address;
      taskCord = Location(lat: 0, lng: 0);
    } catch (e) {}
    try {
      APIViewModel().req<PostTaskFormAPIModel>(
        context: context,
        apiState: APIState(APIType.get_taskform, this.runtimeType, null),
        url: APIPostTaskCfg.TASK_CAT_FORM_GET_URL,
        param: {"userTaskItemId": widget.model.id},
        reqType: ReqType.Get,
      );
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        bottomNavigationBar: drawBottomBar(),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  @override
  drawLayout() {
    return (listTaskItemFormModel.length > 0)
        ? PageView.builder(
            controller: controller,
            itemCount: listTaskItemFormModel.length,
            itemBuilder: (BuildContext context, int index) {
              final modal = listTaskItemFormModel[index];
              //final index2 = index;
              if (modal.answerType == "CheckBox") {
                //listCB.clear();
                return drawCheckBox(modal, index, 1);
              } else if (modal.answerType == "RadioBox") {
                //listRB.clear();
                return drawRadioBox(modal, index, modal.order);
                //return Container();
                //return drawCheckBox(modal, index, 2);
              } else if (modal.answerType == "WorkerNumberBox") {
                return drawWorkerNumberBox(modal);
              } else if (modal.answerType == "DateTimeBox") {
                return drawDateTimeBox(modal);
              } else if (modal.answerType == "LocationBox") {
                return drawLocationBox(modal);
              } else if (modal.answerType == "BudgetBox") {
                return drawBudgetBox(modal);
              } else if (modal.answerType == "TextArea") {
                return drawTextArea(modal);
              } else {
                return Container();
              }
            })
        : Container();
  }

  @override
  onGetStarted() {
    try {
      for (int index = 0; index < listTaskItemFormModel.length; index++) {
        final model = listTaskItemFormModel[index];
        //  CHECKBOX PARSER FOR ANSWERS
        if (model.answerType == "CheckBox") {
          var answers = "";
          if (isLng(AppDefine.GEO_CODE)) {
            answers = model.answersBn;
          } else {
            answers = model.answers;
          }
          final arr = answers.split(',');

          for (var order in optController.listCB.keys) {
            if (order == model.order) {
              var str = "";
              TextEditingController input;
              final list = optController.listCB[order];
              try {
                input = inputOthers[index];
              } catch (e) {}
              for (var j = 0; j < list.length; j++) {
                final map = list[j];
                if (map[j]) {
                  if (arr[j].toLowerCase() == "others".tr && input != null) {
                    final txt = input.text.trim().replaceAll(",", "");
                    str += txt;
                    str += ", ";
                  } else {
                    str += arr[j];
                    str += ", ";
                  }
                }
              }
              if (str != null && str.length > 0) {
                str = str.substring(0, str.length - 2);
              }
              print(str);
              model.seletedAnswer = str;
            }
          }
        }
        //  RADIOBUTTON PARSER FOR ANSWERS
        else if (model.answerType == "RadioBox") {
          var answers = "";
          if (isLng(AppDefine.GEO_CODE)) {
            answers = model.answersBn;
          } else {
            answers = model.answers;
          }
          final arr = answers.split(',');
          //int len = optController.listRB.length;
          for (var order in optController.listRB.keys) {
            if (order == model.order) {
              var str = "";
              TextEditingController input;
              final v = optController.listRB[order];
              try {
                input = inputOthers[index];
              } catch (e) {}
              final txt = arr[v];
              if (txt.toLowerCase() == "others".tr && input != null) {
                final txt = input.text.trim().replaceAll(",", "");
                str += txt;
              } else {
                str += txt;
              }
              print(str);
              model.seletedAnswer = str;
            }
          }
        } else if (model.answerType == "WorkerNumberBox") {
          model.seletedAnswer = totalTaskers.toString();
        } else if (model.answerType == "DateTimeBox") {
          model.seletedAnswer = dueDate;
        } else if (model.answerType == "LocationBox") {
          model.seletedAnswer = taskAddress;
        } else if (model.answerType == "BudgetBox") {
          model.seletedAnswer = estBudget.text.trim();
        } else if (model.answerType == "TextArea") {
          model.seletedAnswer = cmt.text.trim();
        }

        model.latitue = taskCord.lat ?? 0;
        model.longitude = taskCord.lng ?? 0;
        model.taskId = 0;
      }

      final jsonString = JsonString(json.encode(listTaskItemFormModel));
      myLog(jsonString.source);

      final param = json.decode(json.encode(listTaskItemFormModel));
      APIViewModel().req<SubmitTaskFormAPIModel>(
          context: context,
          url: APIPostTaskCfg.TASK_CAT_FORM_POST_URL,
          reqType: ReqType.Post,
          param: param,
          callback: (model) {
            if (model != null && mounted) {
              if (model.success) {
                Get.to(() => DoneTaskPage(model: model)).then((value) {
                  Get.back(result: true);
                });
                /*showAlert(context:context,
                    msg: model.messages.task_post[0], isToast: true, which: 1);
                Future.delayed(Duration(seconds: 1), () {
                  Get.to(() => DoneTaskPage(model: model));
                });*/
              } else {
                showToast(
                    context: context,
                    msg: model.errorMessages.task_post[0],
                    which: 0);
              }
            }
          });
    } catch (e) {
      print(e.toString());
    }
  }
}
