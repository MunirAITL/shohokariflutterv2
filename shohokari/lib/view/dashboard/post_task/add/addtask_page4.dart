import 'package:aitl/config/server/APIEmailNotiCfg.dart';
import 'package:aitl/config/server/APIPostTaskCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/auth/UserModel.dart';
import 'package:aitl/data/model/dashboard/posttask/taskpic/SavePicAPIModel.dart';
import 'package:aitl/data/model/misc/CommonAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/post_task/add/addtask_base.dart';
import 'package:aitl/view/widgets/btn/BottomBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AddTask4Page extends StatefulWidget {
  final String dueDate;
  final UserModel userModel;
  final bool isRequestQuote;
  const AddTask4Page({
    Key key,
    @required this.dueDate,
    @required this.userModel,
    this.isRequestQuote = false,
  }) : super(key: key);
  @override
  State createState() => _AddTask4PageState();
}

const minEstBudget = 50;
const maxEstBudget = 1000000;

class _AddTask4PageState extends BaseAddTaskStatefull<AddTask4Page>
    with APIStateListener {
  //  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.email_noti &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {} catch (e) {}
          }
        }
      } else if (apiState.type == APIType.save_pic &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {}
        }
      }
    } catch (e) {
      myLog(e.toString());
    }
  }

  uploadTaskImages(int pageNo) async {
    try {
      for (var map in taskController.getTaskModel().taskImages) {
        if (taskController.getTaskModel().id != null) {
          await APIViewModel().req<SavePicAPIModel>(
            context: context,
            apiState: APIState(APIType.save_pic, this.runtimeType, null),
            url: APIPostTaskCfg.SAVE_PIC_URL,
            reqType: ReqType.Post,
            param: {
              "EntityId": taskController.getTaskModel().id,
              "EntityName": "Task",
              "PropertyName": "Task",
              "Value": map['id'],
            },
          );
        }
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      taskController.setTaskModel(null);
      taskController.dispose();
    } catch (e) {}

    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }

    try {
      taskController.dispose();
    } catch (e) {}

    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    try {
      //  to save pic api
      uploadTaskImages(4);

      //  email notification api call
      await APIViewModel().req<CommonAPIModel>(
        context: context,
        apiState: APIState(APIType.email_noti, this.runtimeType, null),
        url: APIEmailNotiCfg.TASK_EMAI_NOTI_GET_URL.replaceAll(
            "#taskId#", taskController.getTaskModel().id.toString()),
        isLoading: false,
        reqType: ReqType.Get,
      );
    } catch (e) {}
  }

  onNextClicked() {}
  onDelTaskClicked() {}

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          automaticallyImplyLeading: false,
          title: UIHelper()
              .drawAppbarTitle(title: 'offer_accepted_confirmation_title'.tr),
          centerTitle: false,
        ),
        bottomNavigationBar: drawBottomBtn(
            context: context,
            text: "done".tr,
            callback: () async {
              taskController.setTaskModel(null);
              Get.back(result: 4);
            }),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(height: 20),
              Txt(
                txt: "post_task_finished_title_message"
                    .tr, //+ widget.userModel.firstName + ",",
                txtColor: MyTheme.gray5Color,
                txtSize: MyTheme.txtSize + 1.3,
                txtAlign: TextAlign.center,
                isBold: false,
              ),
              SizedBox(height: 20),
              Txt(
                txt: "post_task_finished_your_task_was_posted_text".tr,
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false,
              )
            ],
          ),
        ),
      ),
    );
  }
}
