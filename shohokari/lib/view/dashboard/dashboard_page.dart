import 'package:aitl/config/app/events/DeviceEventTypesCfg.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/network/CookieMgr.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/auth/auth_screen.dart';
import 'package:aitl/view/dashboard/findworks/find_works_page.dart';
import 'package:aitl/view/dashboard/messages/private_msg_page.dart';
import 'package:aitl/view/dashboard/more/more_page.dart';
import 'package:aitl/view/dashboard/mytasks/mytasks_page.dart';
import 'package:aitl/view/dashboard/post_task/post_task_page.dart';
import 'package:aitl/view/widgets/botnav/bottomNavigation.dart';
import 'package:aitl/view/widgets/botnav/tabItem.dart';
import 'package:aitl/view/widgets/dialog/HelpTutDialog.dart';
import 'package:aitl/view_model/helper/utils/APIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl/view_model/rx/BotNavController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

//  next on https://pub.dev/packages/persistent_bottom_nav_bar

class DashboardPage extends StatefulWidget {
  static const int TAB_POSTTASK = 0;
  static const int TAB_MYTASK = 1;
  static const int TAB_FINDWORKS = 2;
  static const int TAB_MESSAGES = 3;
  static const int TAB_MORE = 4;

  @override
  State createState() => DashboardPageState();
}

class DashboardPageState extends State<DashboardPage>
    with Mixin, APIStateListener, StateListener {
  final List<Widget> listTabbar = [];
  static bool isDialogHelpOpenned = false;

  final botNavController = Get.put(BotNavController());

  //static int currentTab = 0;

  List<TabItem> tabs = [
    TabItem(
      tabName: "tab_new_task",
      icon: AssetImage("assets/images/tabbar/ic_new_case.png"),
      //page: NewCaseTab(),
      page: PostTaskListPage(),
    ),
    TabItem(
      tabName: "tab_mytasks",
      icon: AssetImage("assets/images/tabbar/ic_task.png"),
      page: MyTasksPage(),
    ),
    TabItem(
      tabName: "tab_findwork",
      icon: AssetImage("assets/images/tabbar/ic_search.png"),
      page: FindWorksPage(),
    ),
    TabItem(
      tabName: "tab_msg",
      icon: AssetImage("assets/images/tabbar/ic_message.png"),
      page: PrivateMsgPage(),
    ),
    TabItem(
      tabName: "tab_more",
      icon: AssetImage("assets/images/tabbar/ic_more.png"),
      page: MorePage(),
    )
  ];

  DashboardPageState() {
    tabs.asMap().forEach((index, details) {
      details.setIndex(index);
    });
  }

  void _selectTab(int index) {
    //if (index == botNavController.index.value) {
    // pop to first route
    // if the user taps on the active tab
    //tabs[index].key.currentState.popUntil((route) => route.isFirst);
    //setState(() {});
    //} else {
    // update the state
    // in order to repaint

    if (mounted) {
      if (index == 0) {
        StateProvider().notify(ObserverState.STATE_RELOAD_TAB,
            PostTaskListPageState().runtimeType);
      } else if (index == 1) {
        StateProvider().notify(
            ObserverState.STATE_RELOAD_TAB, MyTasksPageState().runtimeType);
      } else if (index == 2) {
        StateProvider().notify(
            ObserverState.STATE_RELOAD_TAB, FindWorksPageState().runtimeType);
      } else if (index == 3) {
        StateProvider().notify(
            ObserverState.STATE_RELOAD_TAB, PrivateMsgPageState().runtimeType);
      } else if (index == 4) {
        StateProvider().notify(
            ObserverState.STATE_RELOAD_TAB, MorePageState().runtimeType);
      }
      setState(() => botNavController.index.value = index);
    }
    //}
  }

  StateProvider _stateProvider;
  @override
  onStateChanged(ObserverState state, dynamic data) {
    try {
      if (state == ObserverState.STATE_OPEN_TAB) {
        botNavController.isShowHelpDialogExtraHand.value = false;
        _selectTab(data ?? 0);
      }
      if (state == ObserverState.STATE_RELOAD_TAB) {
        _selectTab(data ?? 0);
      } /*else if (state == ObserverState.STATE_RELOAD_SOUND) {
        _selectTab(data ?? 0);
      }*/
      else if (state == ObserverState.STATE_LOGOUT) {
        CookieMgr().delCookiee();
        DBMgr.shared.delTable("User");
        Get.offAll(
          () => AuthScreen(),
        ).then((value) {
          //callback(route);
        });
      } else if (state == ObserverState.STATE_OPEN_HELP_DIALOG) {
        if (!isDialogHelpOpenned) {
          isDialogHelpOpenned = true;
          botNavController.isShowHelpDialogExtraHand.value = false;
          _selectTab(DashboardPage.TAB_POSTTASK);
          Get.dialog(HelpTutDialog()).then((value) {
            setState(() {
              isDialogHelpOpenned = false;
              _selectTab(DashboardPage.TAB_FINDWORKS);
            });
          });
        }
      }
    } catch (e) {}
  }

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.pending_review_rating_userid &&
          apiState.cls == this.runtimeType) {}
    } catch (e) {
      myLog(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    try {
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    try {
      botNavController.dispose();
    } catch (e) {}
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    //setTabTitle();
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}

    try {
      if (userData.userModel.isFirstLogin) {
        await APIHelper().wsUserDevice(
            context: context,
            eventType: DeviceEventTypesCfg.USERDEVICE_EVENTTYPE_INSTALL,
            callback: (model) {});
        Future.delayed(Duration.zero, () {
          StateProvider().notify(ObserverState.STATE_OPEN_HELP_DIALOG, null);
        });
      }
    } catch (e) {}
    try {
      //await APIHelper().wsFCMDeviceInfo(context, (model) {});
    } catch (e) {}
    /*try {
      APIViewModel().req<PendingReviewRatingByUserIdAPIModel>(
        context: context,
        apiState: APIState(
            APIType.pending_review_rating_userid, this.runtimeType, null),
        url: APIPostTaskCfg.PENDING_REVIEWRATING_BYUSERID_GET_URL
            .replaceAll("#userId#", userData.userModel.id.toString()),
        isLoading: false,
        reqType: ReqType.Get,
      );
    } catch (e) {}*/
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => true,
      child: SafeArea(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.white,
          body: /*WillPopScope(
            onWillPop: () async {
              final isFirstRouteInCurrentTab =
                  !await tabs[botNavController.index.value]
                      .key
                      .currentState
                      .maybePop();
              if (isFirstRouteInCurrentTab) {
                // if not on the 'main' tab
                if (botNavController.index.value != 0) {
                  // select 'main' tab
                  _selectTab(0);
                  // back button handled by app
                  return false;
                }
              }
              // let system handle back button if we're on the first route
              return isFirstRouteInCurrentTab;
            },
            // this is the base scaffold
            // don't put appbar in here otherwise you might end up
            // with multiple appbars on one screen
            // eventually breaking the app
            child:*/
              Scaffold(
            // indexed stack shows only one child
            body: IndexedStack(
              index: botNavController.index.value,
              children: tabs.map((e) => e.page).toList(),
            ),
            // Bottom navigation
            bottomNavigationBar: BottomNavigation(
              context: context,
              onSelectTab: _selectTab,
              botNavController: botNavController,
              tabs: tabs,
              isHelpTut: isDialogHelpOpenned,
              totalMsg: 0,
              totalNoti: userData.userModel.unreadNotificationCount,
            ),
          ),
          //),
        ),
      ),
    );
  }
}
