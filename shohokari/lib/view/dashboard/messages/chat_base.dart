import 'dart:convert';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/timeline/TimelinePostModel.dart';
import 'package:aitl/data/model/dashboard/timeline/comments/CommentsAdditionalAttributesModel.dart';
import 'package:aitl/data/model/dashboard/timeline/comments/UserCommentPublicModelList.dart';
import 'package:aitl/view/dashboard/messages/utils/ChatBubble.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/views/pic_view.dart';
import 'package:aitl/view_model/helper/utils/TimeLineHelper.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/rx/MyTaskController.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:intl/intl.dart';
import 'package:jiffy/jiffy.dart';

abstract class BaseChatStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> with TimeLineHelper {
  final myTaskController = Get.put(MyTaskController());
  bool isPoster = true;
  int index2 = 0;
  String lastMsgDate2;

  drawMessageList({List<TimelinePostModel> listTimelinePostsModel}) {
    return (listTimelinePostsModel != null)
        ? Padding(
            padding: EdgeInsets.only(bottom: getHP(context, 10)),
            child: drawQ(
              context: context,
              myTaskController: myTaskController,
              listTimelinePostsModel: listTimelinePostsModel,
              isPoster: isPoster,
              isReply: false,
              callback: null,
            ),
          )
        : SizedBox();
  }

  Widget buildQMessage(commentsModel, int index) {
    try {
      //UserCommentPublicModelList commentsModel = listCommentsModel[index];
      bool fromMe =
          (commentsModel.user.id == userData.userModel.id) ? false : true;

      if (!fromMe) index2 = index;
      return Padding(
        padding:
            const EdgeInsets.only(top: 10, left: 10, right: 10, bottom: 10),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            getImage(commentsModel.user.profileImageUrl),
            SizedBox(width: 10),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Flexible(
                        flex: 6,
                        child: Txt(
                            txt: commentsModel.user.name ??
                                userData.userModel.name,
                            txtColor: MyTheme.gray4Color,
                            txtSize: MyTheme.txtSize - .2,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      ),
                      SizedBox(width: 20),
                      (fromMe)
                          ? Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: MyTheme.gray5Color.withAlpha(120),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 5, right: 5, top: 3, bottom: 3),
                                child: Text(
                                  "poster".tr,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                            )
                          : SizedBox(),
                    ],
                  ),
                  SizedBox(height: 2),
                  (commentsModel.commentText != null &&
                          commentsModel.commentText != '')
                      ? (commentsModel.commentText.length <
                              AppConfig.textExpandableSize)
                          ? Txt(
                              txt: commentsModel.commentText.toString().trim(),
                              txtColor: MyTheme.gray5Color,
                              txtSize: MyTheme.txtSize - .3,
                              txtAlign: TextAlign.start,
                              isBold: false)
                          : UIHelper().expandableTxt(
                              commentsModel.commentText,
                              MyTheme.gray5Color,
                              Colors.white,
                            )
                      : SizedBox(),
                  SizedBox(height: 2),
                  (commentsModel.additionalData != '')
                      ? GestureDetector(
                          onTap: () {
                            Get.to(() => PicFullView(
                                  title: "full_screen_image_screen_title".tr,
                                  url: commentsModel.additionalData,
                                ));
                          },
                          child: Container(
                            width: getWP(context, 20),
                            height: getWP(context, 20),
                            decoration: BoxDecoration(
                              //color: Colors.black,
                              image: DecorationImage(
                                image: MyNetworkImage.loadProfileImage(
                                    commentsModel.additionalData),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        )
                      : SizedBox(),
                  SizedBox(height: 2),
                  Txt(
                      txt: DateFun.getTimeAgoTxt(commentsModel.dateCreated),
                      txtColor: Colors.grey,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ],
              ),
            ),
          ],
        ),
      );
    } catch (e) {
      return Container();
    }
  }

  Widget buildSingleMessage(
      List<TimelinePostModel> listTimeLineModel, int index, bool isQ) {
    try {
      //  munir->fixing
      final TimelinePostModel timelineModel = listTimeLineModel[index];

      final List<UserCommentPublicModelList> listCommentsModel =
          timelineModel.userCommentPublicModelList ?? [];

      bool fromMe =
          (timelineModel.ownerId == userData.userModel.id) ? true : false;
      if (isQ) fromMe = false;
      bool isOnline = false;
      try {
        var user = timelineModel.userCommentPublicModelList[0].user;
        isOnline = user.isOnline;
      } catch (e) {}

      if (fromMe) index2 = index;

      var additional;
      try {
        //  "{"ResponseData":{"Images":[{"CanComment":false,"DateCreatedLocal":"0001-01-01T00:00:00","DateCreatedUtc":"2021-07-08T17:36:19.3463903Z","FullyLoaded":false,"LikeStatus":0,"Id":87677,"MediaType":0,"MimeType":"image/jpeg","NextMediaId":0,"ThumbnailUrl":"https://herotasker.com:443/api//media/uploadpictures/05636785261b4e87be4d374e3eed3726_1000_1000.jpg","TotalComments":0,"TotalLikes":0,"Url":"https://herotasker.com:443/api//media/uploadpictures/05636785261b4e87be4d374e3eed3726_0_0.jpg","attachmentType":"OTHER"}]},"Success":true}"
        final CommentsAdditionalAttributesModel
            commentsAdditionalAttributesModel =
            CommentsAdditionalAttributesModel.fromJson(
                json.decode(timelineModel.additionalAttributeValue));
        if (commentsAdditionalAttributesModel.responseData.images.length > 0) {
          additional =
              commentsAdditionalAttributesModel.responseData.images[0].url;
        }
      } catch (e) {
        //myLog(e.toString());
      }

      String lastMsgDate;
      DateTime date1;
      try {
        date1 = Jiffy(timelineModel.dateCreatedUtc).dateTime;
        DateTime date2 = DateTime.now();
        if (index < listTimeLineModel.length - 1) {
          final TimelinePostModel timelineModel2 =
              listTimeLineModel[(index + 1)];
          date2 = Jiffy(timelineModel2.dateCreatedUtc).dateTime;
        }
        if (date1.day == date2.day) {
          // myLog(days.toString());
          var inputDate = DateTime.parse(date1.toString());
          var outputFormat = DateFormat('dd-MMMM-yyyy');
          lastMsgDate = outputFormat.format(inputDate);
        } else {}
      } catch (e) {
        //myLog(e.toString());
      }

      bool isSameDate = (lastMsgDate2 == lastMsgDate) ? true : false;
      lastMsgDate2 = lastMsgDate;

      Alignment alignment = fromMe ? Alignment.topRight : Alignment.topLeft;
      Alignment chatArrowAlignment =
          fromMe ? Alignment.topRight : Alignment.topLeft;

      Color chatBgColor;
      Color txtColor;
      if (fromMe) {
        chatBgColor = MyTheme.dGreenColor;
        txtColor = Colors.white;
      } else {
        chatBgColor = MyTheme.ivory_dark;
        txtColor = Colors.black;
      }
      EdgeInsets edgeInsets = fromMe
          ? EdgeInsets.fromLTRB(5, 5, 15, 5)
          : EdgeInsets.fromLTRB(15, 5, 5, 5);
      EdgeInsets margins = fromMe
          ? EdgeInsets.fromLTRB(80, 5, 10, 5)
          : EdgeInsets.fromLTRB(10, 5, 80, 5);

      return Padding(
        padding: const EdgeInsets.only(left: 10, right: 10),
        child: Column(
          children: [
            (!isSameDate && fromMe && lastMsgDate != null)
                ? Padding(
                    padding: const EdgeInsets.all(20),
                    child: Center(
                      child: Txt(
                          txt: lastMsgDate,
                          txtColor: MyTheme.gray3Color,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: false),
                    ),
                  )
                : SizedBox(),
            (timelineModel.message != null && timelineModel.message != '')
                ? Row(
                    children: [
                      (!fromMe)
                          ? Stack(children: [
                              getImage(timelineModel.ownerImageUrl),
                              Positioned(
                                  top: 0,
                                  right: 0,
                                  child: drawCircle(
                                      context: context,
                                      color: (isOnline)
                                          ? MyTheme.onlineColor
                                          : MyTheme.offlineColor,
                                      size: 2))
                            ])
                          : SizedBox(),
                      Expanded(
                        child: Container(
                          color: Colors.transparent,
                          margin: margins,
                          child: Align(
                            alignment: alignment,
                            child: CustomPaint(
                              painter: ChatBubble(
                                  color: chatBgColor,
                                  alignment: chatArrowAlignment),
                              child: Container(
                                margin: EdgeInsets.all(10),
                                child: Stack(
                                  children: <Widget>[
                                    Padding(
                                      padding: edgeInsets,
                                      child: (timelineModel.message.length <
                                              AppConfig.textExpandableSize)
                                          ? Txt(
                                              txt: timelineModel.message
                                                  .toString()
                                                  .trim(),
                                              txtColor: txtColor,
                                              txtSize: MyTheme.txtSize - .3,
                                              txtAlign: TextAlign.start,
                                              isBold: false)
                                          : UIHelper().expandableTxt(
                                              timelineModel.message,
                                              txtColor,
                                              Colors.white,
                                            ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: 5),
                      (fromMe)
                          ? getImage(timelineModel.ownerImageUrl)
                          : SizedBox(),
                    ],
                  )
                : SizedBox(),
            (additional != null)
                ? Row(
                    children: [
                      (!fromMe)
                          ? Stack(children: [
                              getImage(timelineModel.ownerImageUrl),
                              Positioned(
                                  top: 0,
                                  right: 0,
                                  child: drawCircle(
                                      context: context,
                                      color: (isOnline)
                                          ? MyTheme.onlineColor
                                          : MyTheme.offlineColor,
                                      size: 2))
                            ])
                          : SizedBox(),
                      Expanded(
                        child: Container(
                          //color: Colors.black,
                          child: Container(
                            margin: margins,
                            child: Align(
                              alignment: alignment,
                              child: GestureDetector(
                                onTap: () {
                                  Get.to(() => PicFullView(
                                        title:
                                            "full_screen_image_screen_title".tr,
                                        url: additional,
                                      ));
                                },
                                child: Container(
                                  width: getWP(context, 20),
                                  height: getWP(context, 20),
                                  decoration: BoxDecoration(
                                    //color: Colors.black,
                                    image: DecorationImage(
                                      image: MyNetworkImage.loadProfileImage(
                                          additional),
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: 5),
                      (fromMe)
                          ? getImage(timelineModel.ownerImageUrl)
                          : SizedBox(),
                    ],
                  )
                : SizedBox(),
            Align(
              alignment:
                  (fromMe) ? Alignment.bottomRight : Alignment.bottomLeft,
              child: Padding(
                padding: (!fromMe)
                    ? EdgeInsets.only(left: getWP(context, 13))
                    : EdgeInsets.only(right: getWP(context, 13)),
                child: Txt(
                    txt: DateFun.getTimeAgoTxt(timelineModel.dateCreated),
                    txtColor: Colors.grey,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
            ),
            SizedBox(height: 20),
          ],
        ),
      );
    } catch (e) {
      return Container();
    }
  }

  getImage(url) {
    return Container(
      decoration: MyTheme.picEmboseCircleDeco,
      child: CircleAvatar(
        radius: 25.0,
        backgroundColor: Colors.transparent,
        backgroundImage:
            new CachedNetworkImageProvider(MyNetworkImage.checkUrl(url)),
      ),
    );
  }
}
