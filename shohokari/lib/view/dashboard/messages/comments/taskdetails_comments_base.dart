import 'package:aitl/data/model/dashboard/timeline/TimelinePostModel.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view_model/helper/utils/TimeLineHelper.dart';
import 'package:aitl/view_model/rx/MyTaskController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class BaseTaskDetailsCommentsStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> with TimeLineHelper {
  final myTaskController = Get.put(MyTaskController());
  bool isPoster = true;

  drawMessageList({List<TimelinePostModel> listTimelinePostsModel}) {
    return (listTimelinePostsModel != null)
        ? Padding(
            padding: EdgeInsets.only(bottom: getHP(context, 10)),
            child: drawQ(
              context: context,
              myTaskController: myTaskController,
              listTimelinePostsModel: listTimelinePostsModel,
              isPoster: isPoster,
              isReply: false,
              callback: null,
            ),
          )
        : SizedBox();
  }
}
