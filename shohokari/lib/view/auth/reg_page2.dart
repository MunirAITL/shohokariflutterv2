import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/model/auth/profile/RegProfileAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/auth/otp/sms_page2.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/datepicker/DatePickerView.dart';
import 'package:aitl/view/widgets/google/GPlacesView.dart';
import 'package:aitl/view/widgets/input/InputBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/views/SoundLngBox.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/rx/AudioController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_webservice/geolocation.dart';
import 'package:intl/intl.dart';

import 'auth_base.dart';

class RegView2 extends StatefulWidget {
  @override
  State createState() => _RegView2State();
}

enum genderEnum { male, female }

class _RegView2State extends BaseAuth<RegView2> with APIStateListener {
  final audioController = Get.put(AudioController());
  final _area = TextEditingController();
  final focusCode = FocusNode();

  //  reg2
  String dob = "";
  String regAddr = "";
  Location loc;
  genderEnum _gender = genderEnum.male;

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.reg2 && apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              await DBMgr.shared.setUserProfile(
                user: model.responseData.user,
              );
              await userData.setUserModel();
            } catch (e) {
              myLog(e.toString());
            }
            if (model.responseData.user.isMobileNumberVerified) {
              Get.off(() => DashboardPage());
            } else {
              Get.to(
                () => Sms2Page(
                  mobile: userData.userModel.mobileNumber,
                ),
              ).then((value) {
                //callback(route);
              });
            }
          } else {
            try {
              final err = model.messages.postUser[0].toString();
              showToast(context: context, msg: err);
            } catch (e) {
              myLog(e.toString());
            }
          }
        }
      }
    } catch (e) {
      myLog(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    _area.dispose();
    //_compName.dispose();
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
  }

  validate() {
    if (!UserProfileVal().isDOBOK(context, dob)) {
      return false;
    } else if (regAddr == "") {
      showToast(context: context, msg: "pick_addr".tr);
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    final DateTime dateNow = DateTime.now();
    return SafeArea(
      //  ******** On RegView2
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          elevation: MyTheme.appbarElevation,
          automaticallyImplyLeading: false,
          title: UIHelper().drawAppbarTitle(title: 'create_profile'.tr),
          centerTitle: false,
          actions: drawSoundLngBox(this.runtimeType, audioController, []),
        ),
        body: Container(
          width: getW(context),
          height: getH(context),
          color: Colors.white,
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 30),
                  //drawCompSwitch(),
                  Txt(
                      txt: "confirm_your_offer_add_date_of_birth".tr,
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(height: 5),
                  Txt(
                      txt: "create_profile_dob_info".tr,
                      txtColor: Colors.grey,
                      txtSize: MyTheme.txtSize - .3,
                      txtAlign: TextAlign.start,
                      isBold: true),
                  SizedBox(height: 10),

                  DatePickerView(
                    cap: null,
                    borderWidth: 1,
                    dt: (dob == '')
                        ? 'post_task_schedule_select_due_date'.tr
                        : dob,
                    initialDate:
                        DateTime(dateNow.year - 18, dateNow.month, dateNow.day),
                    firstDate: DateTime(
                        dateNow.year - 100, dateNow.month, dateNow.day),
                    lastDate:
                        DateTime(dateNow.year - 18, dateNow.month, dateNow.day),
                    callback: (value) {
                      if (mounted) {
                        setState(() {
                          try {
                            dob = DateFormat(DateFun.getDOBFormat())
                                .format(value)
                                .toString();
                          } catch (e) {
                            myLog(e.toString());
                          }
                        });
                      }
                    },
                  ),
                  SizedBox(height: 30),
                  drawGender(),
                  SizedBox(height: 20),
                  Txt(
                      txt: "address".tr,
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(height: 5),
                  InputBox(
                    context: context,
                    ctrl: _area,
                    lableTxt: "create_profile_your_location_label".tr,
                    kbType: TextInputType.streetAddress,
                    inputAction: TextInputAction.done,
                    focusNode: focusCode,
                    len: 255,
                  ),
                  GPlacesView(
                      title: null,
                      address: regAddr,
                      callback: (String address, Location loc) {
                        regAddr = address;
                        this.loc = loc;
                        setState(() {});
                      }),
                  SizedBox(height: 50),
                  MMBtn(
                    txt: "on_boarding_poster_four_continue_button_label".tr,
                    width: getW(context),
                    height: getHP(context, MyTheme.btnHpa),
                    radius: 10,
                    callback: () {
                      if (validate()) {
                        APIViewModel().req<RegProfileAPIModel>(
                            context: context,
                            apiState:
                                APIState(APIType.reg2, this.runtimeType, null),
                            url: APIAuthCfg.REG_PROFILE_PUT_URL,
                            reqType: ReqType.Put,
                            param: {
                              "Address": (_area.text.trim() + ' ' + regAddr)
                                  .toString()
                                  .trim(),
                              "BriefBio": '',
                              "CommunityId": userData.communityId,
                              "DateofBirth": dob,
                              "Email": userData.userModel.email,
                              "FirstName": userData.userModel.firstName,
                              "Cohort": (_gender == genderEnum.male)
                                  ? "Male"
                                  : "Female",
                              "Headline": '',
                              "Id": userData.userModel.id,
                              "LastName": userData.userModel.lastName,
                              "Latitude": loc.lat,
                              "Longitude": loc.lng,
                              "MobileNumber": userData.userModel.mobileNumber,
                            });
                      }
                    },
                  ),
                  SizedBox(height: 50),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  drawGender() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Txt(
            txt: "create_profile_gender".tr,
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize,
            txtAlign: TextAlign.center,
            isBold: false,
          ),
          Theme(
            data: Theme.of(context).copyWith(
              unselectedWidgetColor: Colors.black,
              disabledColor: Colors.black,
              selectedRowColor: Colors.black,
              indicatorColor: Colors.black,
              toggleableActiveColor: Colors.black,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    if (mounted) {
                      setState(() {
                        _gender = genderEnum.male;
                      });
                    }
                  },
                  child: Radio(
                    value: genderEnum.male,
                    groupValue: _gender,
                    onChanged: (genderEnum value) {
                      if (mounted) {
                        setState(() {
                          _gender = value;
                        });
                      }
                    },
                  ),
                ),
                Txt(
                  txt: "create_profile_gender_type_male".tr,
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.center,
                  isBold: false,
                ),
                InkWell(
                  onTap: () {
                    if (mounted) {
                      setState(() {
                        _gender = genderEnum.female;
                      });
                    }
                  },
                  child: Radio(
                    value: genderEnum.female,
                    groupValue: _gender,
                    onChanged: (genderEnum value) {
                      if (mounted) {
                        setState(() {
                          _gender = value;
                        });
                      }
                    },
                  ),
                ),
                Txt(
                  txt: "create_profile_gender_type_female".tr,
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.center,
                  isBold: false,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
