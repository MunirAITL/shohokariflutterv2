import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:aitl/mixin.dart';

drawStepBar({
  BuildContext context,
  bool isDots = false,
  bool isSelected = false,
  int pos = 0,
  String txt,
  Color txtColor = Colors.black,
  bool isBold = true,
  double dotsLen,
}) {
  return Container(
    //color: Colors.black,
    child: Row(
      children: [
        (isDots)
            ? Padding(
                padding: const EdgeInsets.only(right: 10),
                child: DottedLine(
                  direction: Axis.horizontal,
                  lineLength: dotsLen,
                  lineThickness: 1,
                  dashColor: Colors.grey,
                  dashGapLength: 0.5,
                ),
              )
            : SizedBox(),
        /*SvgPicture.asset(
          'assets/images/svg/posttask/' + icon + '.svg',
          color: MyTheme.blueColor,
        ),*/
        drawCircle(context, Icons.check, isBold, isSelected, pos),
        Padding(
          padding: const EdgeInsets.only(left: 10),
          child: Txt(
            txt: txt,
            txtColor: txtColor,
            txtSize: MyTheme.txtSize - .2,
            txtAlign: TextAlign.center,
            isBold: isBold,
            isOverflow: true,
          ),
        ),
      ],
    ),
  );
}

drawCircle(context, IconData icon, bool isBold, bool isSelected, int pos) {
  double w = MediaQuery.of(context).size.width / 20;
  return Container(
    width: w,
    height: w,
    child: (isBold || isSelected)
        ? Center(
            child: Icon(
              icon,
              size: 15,
              color: (isBold || isSelected) ? Colors.white : Colors.black,
            ),
          )
        : Center(
            child: Text(pos.toString(),
                style: TextStyle(color: Colors.white, fontSize: 12)),
          ),
    decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: (isBold || isSelected) ? MyTheme.blueColor : Colors.grey),
  );
}
