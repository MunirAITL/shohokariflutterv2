import 'dart:ui';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view_model/helper/utils/HelpTutHelper.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl/view_model/rx/BotNavController.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:aitl/mixin.dart';
import '../txt/Txt.dart';

class HelpTutDialog extends StatefulWidget {
  @override
  State createState() => _HelpTutDialogState();
}

class _HelpTutDialogState extends State<HelpTutDialog> with Mixin {
  int index = 0;
  final botNavController = Get.put(BotNavController());

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    HelpTutHelper().listTips = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (index < 0 || index >= HelpTutHelper().listTips.length) Get.back();
    final iconSize = 80.0;
    return Column(
      // crossAxisAlignment: CrossAxisAlignment.end,
      //mainAxisAlignment:
      //index == 0 ? MainAxisAlignment.end : MainAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Dialog(
          backgroundColor: Colors.transparent,
          elevation: 0,
          insetPadding: EdgeInsets.only(
              left: 50,
              right: 50,
              bottom: getHP(context,
                  17)), //index == 0 ? getHP(context, 35) : getHP(context, 20)),

          child: GestureDetector(
            onTap: () {
              botNavController.isShowHelpDialogExtraHand.value = false;
              if (index == HelpTutHelper().listTips.length - 1) Get.back();
            },
            child: Container(
              margin: EdgeInsets.only(left: 0.0, right: 0.0),
              child: Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Material(
                    elevation: 0,
                    color: Colors.transparent,
                    child: Container(
                      padding: EdgeInsets.only(
                        top: 18.0,
                      ),
                      margin: EdgeInsets.only(top: 30, right: 10),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                            width: 1,
                            color: MyTheme.brandColor,
                            style: BorderStyle.solid,
                          ),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                              color: Colors.black26,
                              blurRadius: 0.0,
                              offset: Offset(0.0, 0.0),
                            ),
                          ]),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 20, left: 20, right: 20),
                            child: Txt(
                              txt: index < HelpTutHelper().listTips.length
                                  ? HelpTutHelper().listTips[index]
                                  : '',
                              txtColor: MyTheme.redColor,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.center,
                              isBold: true,
                              //txtLineSpace: 1.5,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(10),
                            child: Container(
                              decoration: BoxDecoration(
                                color:
                                    index != HelpTutHelper().listTips.length - 1
                                        ? Colors.transparent
                                        : MyTheme.brandColor,
                                borderRadius: BorderRadius.circular(20),
                              ),
                              height: 50,
                              width: double.infinity,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  GestureDetector(
                                    onTap: () {
                                      if (index != 0) {
                                        setState(() {
                                          index--;
                                          if (index < 1) index = 0;
                                          _switchTabsHand(index);
                                        });
                                      }
                                    },
                                    child: Container(
                                      width: getWP(context, 20),
                                      color: Colors.transparent,
                                      child: Transform.translate(
                                        offset: Offset(0, -15),
                                        child: Icon(
                                          Icons.arrow_left,
                                          color: (index == 0)
                                              ? Colors.grey
                                              : MyTheme.brandColor,
                                          size: iconSize,
                                        ),
                                      ),
                                    ),
                                  ),
                                  index == HelpTutHelper().listTips.length - 1
                                      ? Expanded(
                                          child: Txt(
                                            txt: "finish"
                                                .tr, //HelpTutHelper().listTab[index].toString(),
                                            txtColor: Colors.white,
                                            txtSize: MyTheme.txtSize,
                                            txtAlign: TextAlign.center,
                                            isBold: false,
                                          ),
                                        )
                                      : SizedBox(),
                                  (index == HelpTutHelper().listTips.length - 1)
                                      ? SizedBox(width: iconSize)
                                      : GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              index++;
                                              if (index >
                                                  HelpTutHelper()
                                                      .listTips
                                                      .length)
                                                index = HelpTutHelper()
                                                    .listTips
                                                    .length;
                                              _switchTabsHand(index);
                                            });
                                          },
                                          child: Container(
                                            width: getWP(context, 20),
                                            color: Colors.transparent,
                                            child: Transform.translate(
                                              offset: Offset(0, -15),
                                              child: Icon(
                                                Icons.arrow_right,
                                                color: MyTheme.brandColor,
                                                size: iconSize,
                                              ),
                                            ),
                                          ),
                                        ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    top: 5,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Container(
                            //width: 50.0,
                            //height: 50.0,
                            padding: const EdgeInsets.all(
                                10), //I used some padding without fixed width and height
                            decoration: new BoxDecoration(
                              shape: BoxShape
                                  .circle, // You can use like this way or like the below line
                              //borderRadius: new BorderRadius.circular(30.0),
                              color: Colors.white,
                              border: Border.all(
                                width: .5,
                                color: MyTheme.brandColor,
                                style: BorderStyle.solid,
                              ),
                            ),
                            child: Icon(Icons.help,
                                color: MyTheme.brandColor, size: 30)),
                      ),
                    ),
                  ),
                  Positioned(
                    top: 15,
                    right: 0,
                    child: Align(
                      alignment: Alignment.topRight,
                      child: GestureDetector(
                          child: Container(
                            //width: 50.0,
                            //height: 50.0,
                            padding: const EdgeInsets.all(
                                2), //I used some padding without fixed width and height
                            decoration: new BoxDecoration(
                              shape: BoxShape
                                  .circle, // You can use like this way or like the below line
                              //borderRadius: new BorderRadius.circular(30.0),
                              color: Colors.white,
                              border: Border.all(
                                width: 1,
                                color: MyTheme.brandColor,
                                style: BorderStyle.solid,
                              ),
                            ),
                            child: Icon(
                              Icons.close,
                              color: MyTheme.brandColor,
                            ),
                          ),
                          onTap: () {
                            Get.back();
                          }),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  _switchTabsHand(int index2) {
    if (index2 == HelpTutHelper().listTips.length - 1) {
      botNavController.isShowHelpDialogExtraHand.value = true;
      StateProvider().notify(ObserverState.STATE_BOTNAV, null);
    } else {
      botNavController.isShowHelpDialogExtraHand.value = false;
      StateProvider().notify(ObserverState.STATE_RELOAD_TAB, index2);
    }
  }
}
