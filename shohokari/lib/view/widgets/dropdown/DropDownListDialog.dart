import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:flutter/material.dart';

// ignore: non_constant_identifier_names
DropDownListDialog({
  BuildContext context,
  String title,
  Color titleColor,
  DropListModel ddTitleList,
  Function(OptionItem optionItem) callback,
}) {
  Color txtColor = (titleColor != null) ? titleColor : Colors.grey;
  ddTitleList.listOptionItems.forEach((element) {
    if (element.title == title) {
      txtColor = Colors.black;
      return;
    }
  });

  return Container(
    padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 5),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border(
            left: BorderSide(color: Colors.grey, width: 1),
            right: BorderSide(color: Colors.grey, width: 1),
            top: BorderSide(color: Colors.grey, width: 1),
            bottom: BorderSide(color: Colors.grey, width: 1)),
        color: Colors.transparent),
    child: GestureDetector(
      onTap: () {
        showGeneralDialog(
            barrierColor: Colors.black.withOpacity(0.2),
            transitionBuilder: (context, a1, a2, widget) {
              return Transform.scale(
                scale: a1.value,
                child: Opacity(
                  opacity: a1.value,
                  child: Dialog(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    backgroundColor: Colors.white,
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: ddTitleList.listOptionItems.length,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () {
                            callback(ddTitleList.listOptionItems[index]);
                            Navigator.of(context, rootNavigator: true).pop();
                          },
                          child: ListTile(
                            title: Card(
                                elevation: .5,
                                child: Padding(
                                    padding: const EdgeInsets.only(
                                        top: 5, bottom: 5),
                                    child: Txt(
                                        txt:
                                            "${ddTitleList.listOptionItems[index].title}",
                                        txtColor: Colors.black,
                                        txtSize: MyTheme.txtSize,
                                        txtAlign: TextAlign.center,
                                        isBold: false))),
                          ),
                        );
                      },
                    ),
                  ),
                ),
              );
            },
            transitionDuration: Duration(milliseconds: 300),
            barrierDismissible: true,
            barrierLabel: '',
            context: context,
            pageBuilder: (context, animation1, animation2) {});
      },
      child: new Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 20),
              child: Txt(
                  txt: title,
                  txtColor: txtColor,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
          ),
          Align(
            alignment: Alignment(1, 0),
            child: Icon(
              Icons.keyboard_arrow_down,
              color: txtColor,
              size: 40,
            ),
          ),
        ],
      ),
    ),
  );
}
