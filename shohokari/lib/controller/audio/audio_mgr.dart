//import 'package:audioplayers/audio_cache.dart';
//import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/services.dart';
//import 'package:synchronized/synchronized.dart';
//import 'package:assets_audio_player/assets_audio_player.dart';

class AudioMgr {
  static const platform = const MethodChannel('flutter.native/shohokari');
  //AudioPlayer advancedPlayer = AudioPlayer();
  //AudioCache audioCache = new AudioCache();
  //final assetsAudioPlayer = AssetsAudioPlayer();
  //final _lock = new Lock();

  static final AudioMgr shared = AudioMgr._internal();
  factory AudioMgr() {
    return shared;
  }
  AudioMgr._internal();

  play(file) async {
    //if(advancedPlayer.state==S)
    try {
      //await _lock.synchronized(() async {
      //await stopNative();
      playNative(file);

      //audioCache.fixedPlayer = advancedPlayer;

      //  start
      //await stop();
      //await assetsAudioPlayer.open(Audio("assets/audio/bd/$file.mp3"));
      //  end

      //await audioCache.play(
      //'audio/bd/' + file + '.mp3',
      //mode: PlayerMode.LOW_LATENCY,
      //);
      //});
    } catch (e) {}
  }

  stop() async {
    try {
      stopNative();

      //await advancedPlayer.stop();

      //  start
      //await assetsAudioPlayer.stop();
      //  end

      //assetsAudioPlayer.dispose();
      //
    } catch (e) {}
  }

  stop2() async {
    try {
      //stopNative();

      //await advancedPlayer.stop();

      //  start
      //await assetsAudioPlayer.stop();
      //  end

      //assetsAudioPlayer.dispose();
      //
    } catch (e) {}
  }

  playNative(file) async {
    try {
      final param = {'file': file, 'method': 'start'};
      await platform.invokeMethod('audioActivity', param);
    } on PlatformException catch (e) {
      print(e.toString());
    }
  }

  stopNative() async {
    try {
      final param = {'method': 'stop'};
      await platform.invokeMethod('audioActivity', param);
    } on PlatformException catch (e) {
      print(e.toString());
    }
  }
}