import 'package:aitl/data/model/dashboard/more/task_alert/TaskAlertsModel.dart';

class TaskAlertAPIModel {
  bool success;
  ResponseData responseData;

  TaskAlertAPIModel({this.success, this.responseData});

  TaskAlertAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ResponseData {
  TaskAlertKeywordsModel taskAlertKeyword;
  ResponseData({this.taskAlertKeyword});

  ResponseData.fromJson(Map<String, dynamic> json) {
    taskAlertKeyword = json['TaskAlertKeyword'] != null
        ? new TaskAlertKeywordsModel.fromJson(json['TaskAlertKeyword'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.taskAlertKeyword != null) {
      data['TaskAlertKeyword'] = this.taskAlertKeyword.toJson();
    }
    return data;
  }
}
