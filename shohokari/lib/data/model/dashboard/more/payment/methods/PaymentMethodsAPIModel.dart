import 'PaymentMethodsModel.dart';

class PaymentMethodsAPIModel {
  bool success;
  ResponseData responseData;
  PaymentMethodsAPIModel({this.success, this.responseData});

  PaymentMethodsAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ResponseData {
  List<PaymentMethodsModel> paymentMethods;
  ResponseData({this.paymentMethods});

  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['PaymentMethods'] != null) {
      paymentMethods = [];
      json['PaymentMethods'].forEach((v) {
        try {
          paymentMethods.add(new PaymentMethodsModel.fromJson(v));
        } catch (e) {}
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.paymentMethods != null) {
      data['PaymentMethods'] =
          this.paymentMethods.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
