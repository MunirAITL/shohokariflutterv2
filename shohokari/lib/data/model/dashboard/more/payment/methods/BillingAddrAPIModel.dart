import 'package:aitl/data/model/dashboard/more/payment/methods/BillingAddressModel.dart';

class BillingAddrAPIModel {
  bool success;
  ResponseData responseData;
  BillingAddrAPIModel({this.success, this.responseData});

  BillingAddrAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ResponseData {
  BillingAddressModel billingAddress;
  ResponseData({this.billingAddress});
  ResponseData.fromJson(Map<String, dynamic> json) {
    billingAddress = json['BillingAddress'] != null
        ? new BillingAddressModel.fromJson(json['BillingAddress'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.billingAddress != null) {
      data['BillingAddress'] = this.billingAddress.toJson();
    }
    return data;
  }
}
