class ResAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  ResAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  ResAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  Resolution resolution;
  ResponseData({this.resolution});
  ResponseData.fromJson(Map<String, dynamic> json) {
    resolution = json['Resolution'] != null
        ? new Resolution.fromJson(json['Resolution'])
        : null;
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.resolution != null) {
      data['Resolution'] = this.resolution.toJson();
    }
    return data;
  }
}

class Resolution {
  int userId;
  String creationDate;
  int status;
  String title;
  String description;
  String remarks;
  String emailAddress;
  String phoneNumber;
  int initiatorId;
  String serviceDate;
  String resolutionType;
  int parentId;
  String profileImageUrl;
  String profileOwnerName;
  String complainee;
  String complaineeUrl;
  int assigneeId;
  String assigneeName;
  String assignDate;
  String resolvedDate;
  String taskStatus;
  String userComunity;
  String resolutionsUrl;
  int id;

  Resolution(
      {this.userId,
      this.creationDate,
      this.status,
      this.title,
      this.description,
      this.remarks,
      this.emailAddress,
      this.phoneNumber,
      this.initiatorId,
      this.serviceDate,
      this.resolutionType,
      this.parentId,
      this.profileImageUrl,
      this.profileOwnerName,
      this.complainee,
      this.complaineeUrl,
      this.assigneeId,
      this.assigneeName,
      this.assignDate,
      this.resolvedDate,
      this.taskStatus,
      this.userComunity,
      this.resolutionsUrl,
      this.id});

  Resolution.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'] ?? 0;
    creationDate = json['CreationDate'] ?? '';
    status = json['Status'] ?? 0;
    title = json['Title'] ?? '';
    description = json['Description'] ?? '';
    remarks = json['Remarks'] ?? '';
    emailAddress = json['EmailAddress'] ?? '';
    phoneNumber = json['PhoneNumber'] ?? '';
    initiatorId = json['InitiatorId'] ?? 0;
    serviceDate = json['ServiceDate'] ?? '';
    resolutionType = json['ResolutionType'] ?? '';
    parentId = json['ParentId'] ?? 0;
    profileImageUrl = json['ProfileImageUrl'] ?? '';
    profileOwnerName = json['ProfileOwnerName'] ?? '';
    complainee = json['Complainee'] ?? '';
    complaineeUrl = json['ComplaineeUrl'] ?? '';
    assigneeId = json['AssigneeId'] ?? 0;
    assigneeName = json['AssigneeName'] ?? '';
    assignDate = json['AssignDate'] ?? '';
    resolvedDate = json['ResolvedDate'] ?? '';
    taskStatus = json['TaskStatus'] ?? '';
    userComunity = json['UserComunity'] ?? '';
    resolutionsUrl = json['ResolutionsUrl'] ?? '';
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['CreationDate'] = this.creationDate;
    data['Status'] = this.status;
    data['Title'] = this.title;
    data['Description'] = this.description;
    data['Remarks'] = this.remarks;
    data['EmailAddress'] = this.emailAddress;
    data['PhoneNumber'] = this.phoneNumber;
    data['InitiatorId'] = this.initiatorId;
    data['ServiceDate'] = this.serviceDate;
    data['ResolutionType'] = this.resolutionType;
    data['ParentId'] = this.parentId;
    data['ProfileImageUrl'] = this.profileImageUrl;
    data['ProfileOwnerName'] = this.profileOwnerName;
    data['Complainee'] = this.complainee;
    data['ComplaineeUrl'] = this.complaineeUrl;
    data['AssigneeId'] = this.assigneeId;
    data['AssigneeName'] = this.assigneeName;
    data['AssignDate'] = this.assignDate;
    data['ResolvedDate'] = this.resolvedDate;
    data['TaskStatus'] = this.taskStatus;
    data['UserComunity'] = this.userComunity;
    data['ResolutionsUrl'] = this.resolutionsUrl;
    data['Id'] = this.id;
    return data;
  }
}
