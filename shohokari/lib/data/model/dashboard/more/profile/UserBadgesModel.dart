class UserBadgesModel {
  String user;
  String creationDate;
  String description;
  int id;
  bool isVerified;
  String referenceId;
  String referenceType;
  String refrenceUrl;
  String refrenceNumber;
  String refrence;
  int status;
  String title;
  String type;
  int userId;
  String verificationCode;
  String accountNumber;
  String accountName;
  String swiftCode;
  String profileImageUrl;
  String profileOwnerName;

  UserBadgesModel({
    this.user,
    this.creationDate,
    this.description,
    this.id,
    this.isVerified,
    this.referenceId,
    this.referenceType,
    this.refrenceUrl,
    this.refrenceNumber,
    this.refrence,
    this.status,
    this.title,
    this.type,
    this.userId,
    this.verificationCode,
    this.accountNumber,
    this.accountName,
    this.swiftCode,
    this.profileImageUrl,
    this.profileOwnerName,
  });

  UserBadgesModel.fromJson(Map<String, dynamic> json) {
    user = json['User'] ?? '';
    creationDate = json['CreationDate'] ?? '';
    description = json['Description'] ?? '';
    id = json['Id'] ?? 0;
    isVerified = json['IsVerified'] ?? false;
    referenceId = json['ReferenceId'] ?? '';
    referenceType = json['ReferenceType'] ?? '';
    refrenceUrl = json['RefrenceUrl'] ?? '';
    refrenceNumber = json['RefrenceNumber'] ?? '';
    refrence = json['Refrence'] ?? '';
    status = json['Status'] ?? 0;
    title = json['Title'] ?? '';
    type = json['Type'] ?? '';
    userId = json['UserId'] ?? 0;
    verificationCode = json['VerificationCode'] ?? '';
    accountNumber = json['AccountNumber'] ?? '';
    accountName = json['AccountName'] ?? '';
    swiftCode = json['SwiftCode'] ?? '';
    profileImageUrl = json['ProfileImageUrl'] ?? '';
    profileOwnerName = json['ProfileOwnerName'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['User'] = this.user;
    data['CreationDate'] = this.creationDate;
    data['Description'] = this.description;
    data['Id'] = this.id;
    data['IsVerified'] = this.isVerified;
    data['ReferenceId'] = this.referenceId;
    data['ReferenceType'] = this.referenceType;
    data['RefrenceUrl'] = this.refrenceUrl;
    data['RefrenceNumber'] = this.refrenceNumber;
    data['Refrence'] = this.refrence;
    data['Status'] = this.status;
    data['Title'] = this.title;
    data['Type'] = this.type;
    data['UserId'] = this.userId;
    data['VerificationCode'] = this.verificationCode;
    data['AccountNumber'] = this.accountNumber;
    data['AccountName'] = this.accountName;
    data['SwiftCode'] = this.swiftCode;
    data['ProfileImageUrl'] = this.profileImageUrl;
    data['ProfileOwnerName'] = this.profileOwnerName;
    return data;
  }
}
