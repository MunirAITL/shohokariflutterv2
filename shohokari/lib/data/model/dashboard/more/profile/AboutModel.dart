class AboutModel {
  int userId;
  String user;
  String whatIamlookingfor;
  String languages;
  String qualifications;
  String experiences;
  String goAround;
  String remarks;
  int id;

  AboutModel(
      {this.userId,
      this.user,
      this.whatIamlookingfor,
      this.languages,
      this.qualifications,
      this.experiences,
      this.goAround,
      this.remarks,
      this.id});

  AboutModel.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'] ?? 0;
    user = json['User'] ?? '';
    whatIamlookingfor = json['WhatIamlookingfor'] ?? '';
    languages = json['Languages'] ?? '';
    qualifications = json['Qualifications'] ?? '';
    experiences = json['Experiences'] ?? '';
    goAround = json['GoAround'] ?? '';
    remarks = json['Remarks'] ?? '';
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['User'] = this.user;
    data['WhatIamlookingfor'] = this.whatIamlookingfor;
    data['Languages'] = this.languages;
    data['Qualifications'] = this.qualifications;
    data['Experiences'] = this.experiences;
    data['GoAround'] = this.goAround;
    data['Remarks'] = this.remarks;
    data['Id'] = this.id;
    return data;
  }
}
