// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'FiltersModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FiltersModel _$FiltersModelFromJson(Map<String, dynamic> json) {
  return FiltersModel(
    tabNo: json['tabNo'] as int,
    location: json['location'] as String,
    lat: (json['lat'] as num)?.toDouble(),
    lng: (json['lng'] as num)?.toDouble(),
    distance: (json['distance'] as num)?.toInt(),
    minPrice: (json['minPrice'] as num)?.toInt(),
    maxPrice: (json['maxPrice'] as num)?.toInt(),
    isAvailableTasksOnly: json['isAvailableTasksOnly'] as bool,
  );
}

Map<String, dynamic> _$FiltersModelToJson(FiltersModel instance) =>
    <String, dynamic>{
      'tabNo': instance.tabNo,
      'location': instance.location,
      'lat': instance.lat,
      'lng': instance.lng,
      'distance': instance.distance,
      'minPrice': instance.minPrice,
      'maxPrice': instance.maxPrice,
      'isAvailableTasksOnly': instance.isAvailableTasksOnly,
    };
