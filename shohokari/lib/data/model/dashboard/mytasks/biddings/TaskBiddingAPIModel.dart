import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingModel.dart';

class TaskBiddingAPIModel {
  bool success;
  ResponseData responseData;

  TaskBiddingAPIModel({this.success, this.responseData});

  TaskBiddingAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ResponseData {
  TaskBiddingModel taskBidding;
  ResponseData({this.taskBidding});

  ResponseData.fromJson(Map<String, dynamic> json) {
    taskBidding = json['TaskBidding'] != null
        ? new TaskBiddingModel.fromJson(json['TaskBidding'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.taskBidding != null) {
      data['TaskBidding'] = this.taskBidding.toJson();
    }
    return data;
  }
}
