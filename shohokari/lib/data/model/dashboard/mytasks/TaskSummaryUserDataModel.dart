class TaskSummaryUserDataModel {
  int posterTaskDraftCount;
  int posterTaskOpenForBidCount;
  int posterTaskAssignedCount;
  int posterTaskAwaitingPaymentCount;
  int posterTaskCompletedCount;
  int posterTaskOverDueCount;
  int taskBidOnCount;
  int taskAssignedCount;
  int taskAwaitingPaymentCount;
  int taskCompletedCount;
  int taskOverDueCount;
  int id;

  TaskSummaryUserDataModel(
      {this.posterTaskDraftCount,
      this.posterTaskOpenForBidCount,
      this.posterTaskAssignedCount,
      this.posterTaskAwaitingPaymentCount,
      this.posterTaskCompletedCount,
      this.posterTaskOverDueCount,
      this.taskBidOnCount,
      this.taskAssignedCount,
      this.taskAwaitingPaymentCount,
      this.taskCompletedCount,
      this.taskOverDueCount,
      this.id});

  TaskSummaryUserDataModel.fromJson(Map<String, dynamic> json) {
    posterTaskDraftCount = json['PosterTaskDraftCount'] ?? 0;
    posterTaskOpenForBidCount = json['PosterTaskOpenForBidCount'] ?? 0;
    posterTaskAssignedCount = json['PosterTaskAssignedCount'] ?? 0;
    posterTaskAwaitingPaymentCount =
        json['PosterTaskAwaitingPaymentCount'] ?? 0;
    posterTaskCompletedCount = json['PosterTaskCompletedCount'] ?? 0;
    posterTaskOverDueCount = json['PosterTaskOverDueCount'] ?? 0;
    taskBidOnCount = json['TaskBidOnCount'] ?? 0;
    taskAssignedCount = json['TaskAssignedCount'] ?? 0;
    taskAwaitingPaymentCount = json['TaskAwaitingPaymentCount'] ?? 0;
    taskCompletedCount = json['TaskCompletedCount'] ?? 0;
    taskOverDueCount = json['TaskOverDueCount'] ?? 0;
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['PosterTaskDraftCount'] = this.posterTaskDraftCount;
    data['PosterTaskOpenForBidCount'] = this.posterTaskOpenForBidCount;
    data['PosterTaskAssignedCount'] = this.posterTaskAssignedCount;
    data['PosterTaskAwaitingPaymentCount'] =
        this.posterTaskAwaitingPaymentCount;
    data['PosterTaskCompletedCount'] = this.posterTaskCompletedCount;
    data['PosterTaskOverDueCount'] = this.posterTaskOverDueCount;
    data['TaskBidOnCount'] = this.taskBidOnCount;
    data['TaskAssignedCount'] = this.taskAssignedCount;
    data['TaskAwaitingPaymentCount'] = this.taskAwaitingPaymentCount;
    data['TaskCompletedCount'] = this.taskCompletedCount;
    data['TaskOverDueCount'] = this.taskOverDueCount;
    data['Id'] = this.id;
    return data;
  }
}
