import 'TimelinePostModel.dart';

class GetTimeLineByAppAPIModel {
  bool success;
  ResponseData responseData;

  GetTimeLineByAppAPIModel({this.success, this.responseData});

  GetTimeLineByAppAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ResponseData {
  List<TimelinePostModel> timelinePosts;
  ResponseData({this.timelinePosts});
  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['TimelinePosts'] != null) {
      timelinePosts = [];
      json['TimelinePosts'].forEach((v) {
        try {
          timelinePosts.add(new TimelinePostModel.fromJson(v));
        } catch (e) {}
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.timelinePosts != null) {
      data['TimelinePosts'] =
          this.timelinePosts.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
