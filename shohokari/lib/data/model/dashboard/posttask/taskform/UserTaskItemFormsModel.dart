class UserTaskItemFormsModel {
  String answerType;
  String answers;
  String answersBn;
  String initiatorBy;
  bool isApp;
  bool isBase;
  bool isWeb;
  int order;
  String question;
  String questionBn;
  String remarks;
  int tabNumber;
  int userId;
  int userTaskCategoryId;
  int id;
  int userTaskItemId;
  String userTaskItemName;
  int userTaskParentCategoryId;
  int taskId;
  String seletedAnswer;
  double latitue;
  double longitude;

  UserTaskItemFormsModel({
    this.answerType,
    this.answers,
    this.answersBn,
    this.initiatorBy,
    this.isApp,
    this.isBase,
    this.isWeb,
    this.order,
    this.question,
    this.questionBn,
    this.remarks,
    this.tabNumber,
    this.userId,
    this.userTaskCategoryId,
    this.id,
    this.userTaskItemId,
    this.userTaskItemName,
    this.userTaskParentCategoryId,
    this.taskId,
    this.seletedAnswer,
    this.latitue,
    this.longitude,
  });

  UserTaskItemFormsModel.fromJson(Map<String, dynamic> json) {
    answerType = json['AnswerType'] ?? '';
    answers = json['Answers'] ?? '';
    answersBn = json['Answers_Bn'] ?? '';
    initiatorBy = json['InitiatorBy'] ?? '';
    isApp = json['IsApp'] ?? false;
    isBase = json['IsBase'] ?? false;
    isWeb = json['IsWeb'] ?? false;
    order = json['Order'] ?? 0;
    question = json['Question'] ?? '';
    questionBn = json['Question_Bn'] ?? '';
    remarks = json['Remarks'] ?? '';
    tabNumber = json['TabNumber'] ?? 0;
    userId = json['UserId'] ?? 0;
    userTaskCategoryId = json['UserTaskCategoryId'] ?? 0;
    id = json['Id'] ?? 0;
    userTaskItemId = json['UserTaskItemId'] ?? 0;
    userTaskItemName = json['UserTaskItemName'] ?? '';
    userTaskParentCategoryId = json['UserTaskParentCategoryId'] ?? 0;
    taskId = json['TaskId'] ?? 0;
    seletedAnswer = json['SeletedAnswer'] ?? '';
    latitue = json['Latitude'] ?? 0.0;
    longitude = json['Longitude'] ?? 0.0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['AnswerType'] = this.answerType;
    data['Answers'] = this.answers;
    data['Answers_Bn'] = this.answersBn;
    data['InitiatorBy'] = this.initiatorBy;
    data['IsApp'] = this.isApp;
    data['IsBase'] = this.isBase;
    data['IsWeb'] = this.isWeb;
    data['Order'] = this.order;
    data['Question'] = this.question;
    data['Question_Bn'] = this.questionBn;
    data['Remarks'] = this.remarks;
    data['TabNumber'] = this.tabNumber;
    data['UserId'] = this.userId;
    data['UserTaskCategoryId'] = this.userTaskCategoryId;
    data['Id'] = this.id;
    data['UserTaskItemId'] = this.userTaskItemId;
    data['UserTaskItemName'] = this.userTaskItemName;
    data['UserTaskParentCategoryId'] = this.userTaskParentCategoryId;
    data['TaskId'] = this.taskId;
    data['SeletedAnswer'] = this.seletedAnswer;
    data['Latitude'] = this.latitue;
    data['Longitude'] = this.longitude;
    return data;
  }
}
