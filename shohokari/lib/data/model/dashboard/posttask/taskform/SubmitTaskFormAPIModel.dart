class SubmitTaskFormAPIModel {
  ErrorMessages errorMessages;
  Messages messages;
  bool success;
  ResponseData responseData;

  SubmitTaskFormAPIModel(
      {this.errorMessages, this.messages, this.success, this.responseData});

  SubmitTaskFormAPIModel.fromJson(Map<String, dynamic> json) {
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new Messages.fromJson(json['Messages'])
        : null;
    success = json['Success'];
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toMap();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toMap();
    }
    data['Success'] = this.success;
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  List<dynamic> task_post;
  ErrorMessages({this.task_post});
  factory ErrorMessages.fromJson(Map<String, dynamic> j) {
    return ErrorMessages(
      task_post: j['task_post'] ?? [],
    );
  }
  Map<String, dynamic> toMap() => {
        'task_post': task_post,
      };
}

class Messages {
  List<dynamic> task_post;
  Messages({this.task_post});
  factory Messages.fromJson(Map<String, dynamic> j) {
    return Messages(
      task_post: j['task_post'] ?? [],
    );
  }
  Map<String, dynamic> toMap() => {
        'task_post': task_post,
      };
}

class ResponseData {
  Task task;
  ResponseData({this.task});
  ResponseData.fromJson(Map<String, dynamic> json) {
    task = json['Task'] != null ? new Task.fromJson(json['Task']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.task != null) {
      data['Task'] = this.task.toJson();
    }
    return data;
  }
}

class Task {
  String creationDate;
  String deliveryDate;
  String deliveryTime;
  String description;
  double dueAmount;
  int dutDateType;
  int employeeId;
  double fixedBudgetAmount;
  double hourlyRate;
  int id;
  bool isFixedPrice;
  bool isInPersonOrOnline;
  bool isReadMessage;
  String jobCategory;
  double latitude;
  double longitude;
  double netTotalAmount;
  double paidAmount;
  String preferedLocation;
  String requirements;
  String skill;
  String status;
  String taskTitleUrl;
  String title;
  int totalAcceptedNumber;
  int totalBidsNumber;
  int totalCompletedNumber;
  double totalHours;
  String updatedDate;
  int userId;
  int workerNumber;

  Task(
      {this.creationDate,
      this.deliveryDate,
      this.deliveryTime,
      this.description,
      this.dueAmount,
      this.dutDateType,
      this.employeeId,
      this.fixedBudgetAmount,
      this.hourlyRate,
      this.id,
      this.isFixedPrice,
      this.isInPersonOrOnline,
      this.isReadMessage,
      this.jobCategory,
      this.latitude,
      this.longitude,
      this.netTotalAmount,
      this.paidAmount,
      this.preferedLocation,
      this.requirements,
      this.skill,
      this.status,
      this.taskTitleUrl,
      this.title,
      this.totalAcceptedNumber,
      this.totalBidsNumber,
      this.totalCompletedNumber,
      this.totalHours,
      this.updatedDate,
      this.userId,
      this.workerNumber});

  Task.fromJson(Map<String, dynamic> json) {
    creationDate = json['CreationDate'] ?? '';
    deliveryDate = json['DeliveryDate'] ?? '';
    deliveryTime = json['DeliveryTime'] ?? '';
    description = json['Description'] ?? '';
    dueAmount = json['DueAmount'] ?? 0.0;
    dutDateType = json['DutDateType'] ?? 0;
    employeeId = json['EmployeeId'] ?? 0;
    fixedBudgetAmount = json['FixedBudgetAmount'] ?? 0.0;
    hourlyRate = json['HourlyRate'] ?? 0.0;
    id = json['Id'] ?? 0;
    isFixedPrice = json['IsFixedPrice'] ?? false;
    isInPersonOrOnline = json['IsInPersonOrOnline'] ?? false;
    isReadMessage = json['IsReadMessage'] ?? false;
    jobCategory = json['JobCategory'] ?? '';
    latitude = json['Latitude'] ?? 0.0;
    longitude = json['Longitude'] ?? 0.0;
    netTotalAmount = json['NetTotalAmount'] ?? 0.0;
    paidAmount = json['PaidAmount'] ?? 0.0;
    preferedLocation = json['PreferedLocation'] ?? '';
    requirements = json['Requirements'] ?? '';
    skill = json['Skill'] ?? '';
    status = json['Status'] ?? '';
    taskTitleUrl = json['TaskTitleUrl'] ?? '';
    title = json['Title'] ?? '';
    totalAcceptedNumber = json['TotalAcceptedNumber'] ?? 0;
    totalBidsNumber = json['TotalBidsNumber'] ?? 0;
    totalCompletedNumber = json['TotalCompletedNumber'] ?? 0;
    totalHours = json['TotalHours'] ?? 0.0;
    updatedDate = json['UpdatedDate'] ?? '';
    userId = json['UserId'] ?? 0;
    workerNumber = json['WorkerNumber'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['CreationDate'] = this.creationDate;
    data['DeliveryDate'] = this.deliveryDate;
    data['DeliveryTime'] = this.deliveryTime;
    data['Description'] = this.description;
    data['DueAmount'] = this.dueAmount;
    data['DutDateType'] = this.dutDateType;
    data['EmployeeId'] = this.employeeId;
    data['FixedBudgetAmount'] = this.fixedBudgetAmount;
    data['HourlyRate'] = this.hourlyRate;
    data['Id'] = this.id;
    data['IsFixedPrice'] = this.isFixedPrice;
    data['IsInPersonOrOnline'] = this.isInPersonOrOnline;
    data['IsReadMessage'] = this.isReadMessage;
    data['JobCategory'] = this.jobCategory;
    data['Latitude'] = this.latitude;
    data['Longitude'] = this.longitude;
    data['NetTotalAmount'] = this.netTotalAmount;
    data['PaidAmount'] = this.paidAmount;
    data['PreferedLocation'] = this.preferedLocation;
    data['Requirements'] = this.requirements;
    data['Skill'] = this.skill;
    data['Status'] = this.status;
    data['TaskTitleUrl'] = this.taskTitleUrl;
    data['Title'] = this.title;
    data['TotalAcceptedNumber'] = this.totalAcceptedNumber;
    data['TotalBidsNumber'] = this.totalBidsNumber;
    data['TotalCompletedNumber'] = this.totalCompletedNumber;
    data['TotalHours'] = this.totalHours;
    data['UpdatedDate'] = this.updatedDate;
    data['UserId'] = this.userId;
    data['WorkerNumber'] = this.workerNumber;
    return data;
  }
}

class Autogenerated {
  ErrorMessages errorMessages;
  ErrorMessages messages;
  bool success;
  ResponseData responseData;

  Autogenerated(
      {this.errorMessages, this.messages, this.success, this.responseData});

  Autogenerated.fromJson(Map<String, dynamic> json) {
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    success = json['Success'];
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toMap();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toMap();
    }
    data['Success'] = this.success;
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}
