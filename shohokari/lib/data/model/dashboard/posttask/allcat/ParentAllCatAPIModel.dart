import 'package:aitl/data/model/dashboard/posttask/allcat/ParentUserTaskCategorysModel.dart';

class ParentAllCatAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  ParentAllCatAPIModel(
      {this.errorMessages, this.messages, this.success, this.responseData});

  ParentAllCatAPIModel.fromJson(Map<String, dynamic> json) {
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    success = json['Success'];
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    data['Success'] = this.success;
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  List<ParentUserTaskCategorysModel> userTaskCategorys;
  ResponseData({this.userTaskCategorys});
  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['UserTaskCategorys'] != null) {
      userTaskCategorys = [];
      json['UserTaskCategorys'].forEach((v) {
        try {
          userTaskCategorys.add(new ParentUserTaskCategorysModel.fromJson(v));
        } catch (e) {}
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userTaskCategorys != null) {
      data['UserTaskCategorys'] =
          this.userTaskCategorys.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
