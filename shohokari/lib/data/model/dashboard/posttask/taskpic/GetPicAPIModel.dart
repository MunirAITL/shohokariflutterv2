import 'GetPicModel.dart';

class GetPicAPIModel {
  ResponseData responseData;
  bool success;

  GetPicAPIModel({this.responseData, this.success});

  GetPicAPIModel.fromJson(Map<String, dynamic> json) {
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
    success = json['Success'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    data['Success'] = this.success;
    return data;
  }
}

class ResponseData {
  List<GetPicModel> taskPictures;
  ResponseData({this.taskPictures});
  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['TaskPictures'] != null) {
      taskPictures = [];
      json['TaskPictures'].forEach((v) {
        try {
          taskPictures.add(new GetPicModel.fromJson(v));
        } catch (e) {}
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.taskPictures != null) {
      data['TaskPictures'] = this.taskPictures.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
