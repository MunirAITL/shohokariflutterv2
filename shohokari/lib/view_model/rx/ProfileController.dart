import 'package:get/get.dart';

class ProfileController extends GetxController {
  //  badge
  var isBadgeStatus101 = false.obs;

  //  rating
  var aveargeRatingAsTasker = 0.obs;
  var aveargeRatingAsPoster = 0.obs;
  var completionRateAsTasker = 0.obs;
  var completionRateAsPoster = 0.obs;
  var countAsTasker = 0.obs;
  var countAsPoster = 0.obs;

  setBadgeValue(val) {
    isBadgeStatus101 = val;
  }

  setAverageRateAsTasker(val) {
    aveargeRatingAsTasker = val;
  }

  setAverageRateAsPoster(val) {
    aveargeRatingAsPoster = val;
  }

  setCompletionRateAsTasker(val) {
    completionRateAsTasker = val;
  }

  setCompletionRateAsPoster(val) {
    completionRateAsPoster = val;
  }

  setCountAsTasker(val) {
    countAsTasker = val;
  }

  setCountAsPoster(val) {
    countAsPoster = val;
  }
}
