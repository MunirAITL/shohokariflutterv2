import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/data/model/dashboard/posttask/TaskModel.dart';
import 'package:get/get.dart';

class MyTaskController extends GetxController {
  var _taskModel = TaskModel().obs;
  //var taskImages = List().obs;
  setTaskModel(TaskModel model) {
    _taskModel = model.obs;
  }

  TaskModel getTaskModel() {
    return _taskModel.value ?? TaskModel();
  }

  getStatus() {
    return TaskStatusCfg().getSatus(getTaskModel().status);
  }

  getStatusCode() {
    print(getTaskModel().status);
    return TaskStatusCfg().getSatusCode(getTaskModel().status);
  }

  isExists() => _taskModel == null ? false : true;
}
