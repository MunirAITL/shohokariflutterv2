import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/switchview/SwitchBar.dart';
import 'package:aitl/view/widgets/switchview/SwitchView.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/rx/FiltersController.dart';
import 'package:flutter/material.dart';
import 'package:flutter_xlider/flutter_xlider.dart';
import 'package:get/get.dart';

class FiltersHelper {
  drawDistanceSlider(FiltersController filterController) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Txt(
                  txt: 'distance'.tr,
                  txtColor: MyTheme.gray4Color,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              Obx(
                () => Txt(
                    txt: filterController.distanceTxt.value,
                    txtColor: MyTheme.gray4Color,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
            ],
          ),
          FlutterSlider(
            values: [filterController.distance.value.toDouble()],
            max: 6,
            min: 0,
            tooltip: FlutterSliderTooltip(
                textStyle: TextStyle(color: Colors.transparent),
                boxStyle: FlutterSliderTooltipBox(
                    decoration: BoxDecoration(color: Colors.transparent))),
            onDragging: (handlerIndex, lowerValue, upperValue) {
              filterController.distance.value = (lowerValue as num).toInt();
              filterController.setDistanceTxt(filterController.distance.value);
            },
            trackBar: FlutterSliderTrackBar(
              activeTrackBar: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  color: MyTheme.blueColor.withOpacity(0.5)),
            ),
            handler: FlutterSliderHandler(
              decoration: BoxDecoration(),
              child: Icon(
                Icons.circle,
                size: 20,
                color: MyTheme.blueColor,
              ),
            ),
          )
        ],
      ),
    );
  }

  drawPriceSlider(FiltersController filterController) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Txt(
                  txt: 'price'.tr,
                  txtColor: MyTheme.gray4Color,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              Obx(
                () => Txt(
                    txt: filterController.priceTxt.value,
                    txtColor: MyTheme.gray4Color,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
            ],
          ),
          FlutterSlider(
            values: [
              filterController.minPrice.value.toDouble(),
              filterController.maxPrice.value.toDouble()
            ],
            max: 9,
            min: 0,
            rangeSlider: true,
            tooltip: FlutterSliderTooltip(
                textStyle: TextStyle(color: Colors.transparent),
                boxStyle: FlutterSliderTooltipBox(
                    decoration: BoxDecoration(color: Colors.transparent))),
            onDragging: (handlerIndex, lowerValue, upperValue) {
              filterController.minPrice.value = (lowerValue as num).toInt();
              filterController.maxPrice.value = (upperValue as num).toInt();
              filterController.setPriceTxt(filterController.minPrice.value,
                  filterController.maxPrice.value);
            },
            trackBar: FlutterSliderTrackBar(
              activeTrackBar: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  color: MyTheme.blueColor.withOpacity(0.5)),
            ),
            handler: FlutterSliderHandler(
              decoration: BoxDecoration(),
              child: Icon(
                Icons.circle,
                size: 20,
                color: MyTheme.blueColor,
              ),
            ),
            rightHandler: FlutterSliderHandler(
              decoration: BoxDecoration(),
              child: Icon(
                Icons.circle,
                size: 20,
                color: MyTheme.blueColor,
              ),
            ),
          )
        ],
      ),
    );
  }

  drawAvailableTaskOnly(FiltersController filterController) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Txt(
              txt: 'browse_tasks_filter_task_status_description'.tr,
              txtColor: MyTheme.gray5Color.withOpacity(.8),
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: false),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Txt(
                    txt: 'browse_tasks_filter_task_status_sub_description'.tr,
                    txtColor: MyTheme.gray4Color,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              SwitchBar(
                activeColor: MyTheme.blueColor,
                value: filterController.isAvailableTasksOnly.value,
                onChanged: (value) {
                  filterController.isAvailableTasksOnly.value = value;
                },
              )
              /*SwitchBar(
                bgColor: Colors.grey.withOpacity(.5),
                thumbColor: Colors.grey.withOpacity(.9),
                thumbActiveColor: Colors.black,
                value: filterController.isAvailableTasksOnly.value,
                onChanged: (v) {
                  filterController.isAvailableTasksOnly.value = v;
                },
              )*/
            ],
          ),
        ],
      ),
    );
  }
}
