import 'package:aitl/config/cfg/PubnubCfg.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/timeline/pubnub/ChatModel.dart';
import 'package:pubnub/pubnub.dart';

class PubnubMgr {
  PubNub pubnub;
  Channel channel;

  initPubNub({Function(Envelope) callback}) async {
    print("init pub nub call");
    pubnub = PubNub(
        defaultKeyset: Keyset(
            subscribeKey: PubNubCfg.subscribeKey,
            publishKey: PubNubCfg.publishKey,
            uuid: UUID(userData.userModel.id.toString())));
    Subscription subscription =
        pubnub.subscribe(channels: {userData.userModel.id.toString()});
    print('sender id : ${userData.userModel.id}');
    subscription.messages.listen((envelope) {
      print('sent a message full : ${envelope.payload}');
      callback(envelope);
    });
  }

  void postMessage({int receiverId, ChatModel chatModel}) async {
    channel = pubnub.channel(receiverId.toString());
    print('sent a message to : ${receiverId.toString()}');
    await channel.publish({
      'data': chatModel.toJson(),
      'user': userData.userModel.id,
    });
  }

  getHistory({int receiverId}) async {
    channel = pubnub.channel(receiverId.toString());
    var history =
        channel.history(chunkSize: 100, order: ChannelHistoryOrder.descending);
    //await history.more();
    print(history.messages.length); // 50
    // await history.more();
    //print(history.messages.length); // 100
  }

  destroy() {
    try {
      pubnub = null;
      channel = null;
    } catch (e) {}
  }
}
