class NotiStatusCfg {
  static const NOTIFICATION_NAME_SEED_DATABASE_QUESTIONS =
      "seed/questions.json";
  static const NOTIFICATION_NAME_SEED_TIMELINEPOST = "TimelinePost";
  static const NOTIFICATION_NAME_TIMELINEPOSTCOMMENT = "Comment";
  static const NOTIFICATION_NAME_MATCHFORACTIONPOST = "MatchForActionPost";
  static const NOTIFICATION_NAME_MatchForActionPostJoin =
      "MatchForActionPostJoin";
  static const NOTIFICATION_NAME_MatchForActionComment =
      "MatchForActionComment";
  static const NOTIFICATION_NAME_Meetup = "Meetup";
  static const NOTIFICATION_NAME_MeetupJoin = "MeetupJoin";
  static const NOTIFICATION_NAME_MeetupComment = "MeetupComment";
  static const NOTIFICATION_NAME_EventCalendar = "EventCalendar";
  static const NOTIFICATION_NAME_EventCalendarJoin = "EventCalendarJoin";
  static const NOTIFICATION_NAME_EventCalenderComment = "EventCalenderComment";
  static const NOTIFICATION_NAME_Venture = "Venture";
  static const NOTIFICATION_NAME_VentureJoin = "VentureJoin";
  static const NOTIFICATION_NAME_VentureComment = "VentureComment";
  static const NOTIFICATION_NAME_Comment = "Comment";
  static const NOTIFICATION_NAME_UserLocationChange = "UserLocationChange";
  static const NOTIFICATION_NAME_Task = "Task";
  static const NOTIFICATION_NAME_TaskCompleted = "TaskCompleted";
  static const NOTIFICATION_NAME_TaskBidding = "TaskBidding";
  static const NOTIFICATION_NAME_TaskBiddingAccepted = "TaskBiddingAccepted";
  static const NOTIFICATION_NAME_TaskBiddingRequestPayment =
      "TaskBiddingRequestPayment";
  static const NOTIFICATION_NAME_TaskBiddingReceivedPayment =
      "TaskBiddingReceivedPayment";
  static const NOTIFICATION_NAME_TaskBiddingComment = "TaskBiddingComment";
  static const NOTIFICATION_NAME_TaskBiddingAdminComment =
      "TaskBiddingAdminComment";
  static const NOTIFICATION_NAME_TaskPayment = "TaskPayment";
  static const NOTIFICATION_NAME_TaskPaymentConfirmation =
      "TaskPaymentConfirmation";
  static const NOTIFICATION_NAME_UserRating = "UserRating";
  static const NOTIFICATION_NAME_User = "User";
  static const NOTIFICATION_NAME_IpProperties = "IpProperties";
  static const NOTIFICATION_NAME_TimeLinePostsDeleted = "TimeLinePostsDeleted";
  static const NOTIFICATION_NAME_UserPromotion = "UserPromotion";

  static const TestPushNotitification = "TestPushNotitification";
  static const NOTIFICATION_NAME_BROADCAST = "Broadcast";
  static const NOTIFICATION_NAME_Resolution = "Resolution";
}
